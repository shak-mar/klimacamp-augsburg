---
layout: home
title: Start
permalink: /
nav_order: 1
---

<a href="https://www.speicherleck.de/iblech/stuff/augsburg-klimacamp/">
  <img src="/welcome.small.jpeg" width="896" height="504" style="width: 100%; height: auto; aspect-ratio: attr(width) / attr(height)" alt="Schnee im Klimacamp">
</a>

<!--<a href="/adventskalender/">
  <img src="/pages/material/adventskalender-banner.small.png" width="540" height="135" style="width: 100%; height: auto; aspect-ratio: attr(width) / attr(height)" alt="Der Adventskalender das Augsburger Klimacamps">
</a>-->

# Klimacamp neben dem Rathaus -- seit dem 1. Juli 2020

**Aktuell: Wir stehen für Online-Vorträge und Gesprächsrunden, auch für
Schulklassen, zur Verfügung.**

[Bericht in ARD-Tagesthemen](https://www.youtube.com/watch?t=810&v=94LAR0I_BSY&feature=youtu.be){: .btn .btn-purple }
[Bericht in BR Schwaben & Altbayern](https://m.youtube.com/watch?v=9blVGXRtuwk){: .btn .btn-purple }

<style>
  .carousel-container { position: relative; }
  .carousel { overflow-x: scroll; scroll-snap-type: x mandatory; scroll-behaviour: smooth; display: flex; }
  .carousel a { flex-shrink: 0; scroll-snap-align: start; }
</style>

<div class="carousel-container" style="padding-top: 1em">
  <div class="carousel">
    <a href="https://www.speicherleck.de/iblech/stuff/klimacamp-2021-auswahl/photo_2021-02-08_14-45-41.jpg"><img src="https://www.speicherleck.de/iblech/stuff/klimacamp-2021-auswahl/photo_2021-02-08_14-45-41.jpg" width="1280" height="960" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)"></a>
    <a href="https://www.speicherleck.de/iblech/stuff/klimacamp-2021-auswahl/photo_2021-02-20_17-21-18.jpg"><img src="https://www.speicherleck.de/iblech/stuff/klimacamp-2021-auswahl/photo_2021-02-20_17-21-18.jpg" width="606" height="960" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)"></a>
    <a href="https://www.speicherleck.de/iblech/stuff/klimacamp-2021-auswahl/photo_2021-02-21_12-30-51.jpg"><img src="https://www.speicherleck.de/iblech/stuff/klimacamp-2021-auswahl/photo_2021-02-21_12-30-51.jpg" width="1280" height="960" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)"></a>
    <a href="https://www.speicherleck.de/iblech/stuff/klimacamp-2021-auswahl/photo_2021-02-21_12-37-57.jpg"><img src="https://www.speicherleck.de/iblech/stuff/klimacamp-2021-auswahl/photo_2021-02-21_12-37-57.jpg" width="960" height="1280" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)"></a>
    <a href="https://www.speicherleck.de/iblech/stuff/klimacamp-2021-auswahl/video_2021-02-21_23-55-34.mp4"><video controls width="848" height="464" preload="none" poster="https://www.speicherleck.de/iblech/stuff/klimacamp-2021-auswahl/video_2021-02-21_23-55-34.jpeg" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)">
      <source src="https://www.speicherleck.de/iblech/stuff/klimacamp-2021-auswahl/video_2021-02-21_23-55-34.mp4" type="video/mp4">
    </video></a>
  </div>
</div>

<div style="width: 100%; height: auto; display: block; aspect-ratio: 1.7">
<iframe type="text/html" style="width: 100%; height: 100%"
  src="https://www.youtube.com/embed/KGXvJnUI1hw?origin=https://augsburg.klimacamp.eu/"
    frameborder="0"></iframe></div>

Der fünfte Jahrestag des Pariser Klimaabkommens ist verstrichen und wir
befinden uns noch immer weit davon entfernt Politik zu führen, die mit dem
1,5-Grad-Ziel übereinstimmt.

<div style="float: right; padding-left: 1em">
  {% include podlove-button.html %}
</div>

Der Bundestag sollte am 3. Juli 2020 das Kohleeinstiegsgesetz beschließen. Das
war unser Anlass, am 1. Juli das Klimacamp zu errichten: 1½ Jahre
Demonstrationszüge von Fridays for Future waren unzureichend, um diesen
klimapolitischen Wahnsinn zu verhindern. Das Gesetz sieht vor, dass
durch Zahlungen an Kohlekraftwerksbetreiber die Kohlekraft mit Steuergeldern
bis 2038 künstlich am Leben gehalten wird. Nach [Berechnungen von
Wissenschaftler\*innen](https://www.scientists4future.org/defizite-kohleausstiegsgesetz-kvbg-e/)
kann Deutschland das demokratisch beschlossene Pariser Klimaabkommen nicht mehr
einhalten, wenn dieses Gesetz tatsächlich umgesetzt wird.

Aber auch unsere Augsburger Stadtregierung muss Verantwortung übernehmen --
tut sie aber nicht. Sie ist der Hauptadressat unseres Protests. Der
schwarz-grüne Koalitionsvertrag sieht vor, drei Mal so viel CO₂ in der
Atmosphäre zu deponieren, als Augsburg selbst bei einer sehr [wohlwollenden
Rechnung](/co2-budget/) noch zusteht.
Wir fragen: Mit welchem Recht nimmt sich die Stadt Augsburg
heraus, ihr CO₂-Restbudget so sehr zu überschreiten? Wir formulieren [vier
konkrete Forderungen](/forderungen/) an unsere Stadtregierung.

Das Klimacamp wurde von Fridays for Future Augsburg errichtet und ist das erste
seiner Art -- mittlerweile gibt es [mehrere weitere Klimacamps](https://www.klimacamp.eu/)
in anderen Städten. Es ist aber kein reines FFF-Camp, schon an
Tag 1 waren wir Klimagerechtigkeitsaktivist\*innen diverser weiterer Initiativen wie
Ende Gelände, Extinction Rebellion, Greenpeace, Grüne Jugend und
Aja. Auch Einzelpersonen ohne Heimatinitiative machen bei uns mit.
Und wir bleiben. Bis es die Situation zulässt.
#nichtmituns #fightfor1point5 #wircampenbisihrhandelt


## Neuigkeiten

**Update**: Wir sind (Stand 8.5. -- **Tag 312**) immer noch hier. Das
Verwaltungsgericht [bestätigte](/pages/Pressemitteilungen/2020-11-10-PM_Gerichtsurteil.html)
kürzlich unsere Rechtmäßigkeit.
Seit Eröffnung des Camps verbrauchte die Stadt bereits 8 % des ihr zustehenden
[CO₂-Restbudgets](/co2-budget). Mehrere gute Anträge der Opposition, die sich durch das
Klimacamp ergaben, wurden nicht zur Diskussion im Stadtrat zugelassen.

**Aktuell (3.5.)**: Augsburg ist bei der Errichtung des
[Ravensburger](https://ravensburg.klimacamp.eu/) und des
[Passauer Klimacamps](https://passau.klimacamp.eu/)
behilflich.

[Fotos zur freien Verwendung](https://www.speicherleck.de/iblech/stuff/augsburg-klimacamp/){: .btn .btn-green }
[Adventskalender](/adventskalender){: .btn .btn-blue }
[Podcast](/podcast){: .btn .btn-purple }


## Soziale Medien

<img src="/icons/twitter.svg" style="width: 1em; height: 1em">
**[Twitter: @Klimacamp_aux](https://twitter.com/Klimacamp_aux)**<br>
<img src="/icons/telegram.svg" style="width: 1em; height: 1em">
**[Infokanal auf Telegram](https://t.me/klimacamp_augsburg)**<br>
<img src="/icons/instagram.svg" style="width: 1em; height: 1em">
**[Instagram: @klimacamp](https://www.instagram.com/klimacamp/)**<br>
<img src="/icons/facebook.svg" style="width: 1em; height: 1em">
**[Facebook](https://www.facebook.com/Klimacamp-Augsburg-112439020580386/)**


## Was wir konkret machen

Im Sommer organisierten wir ein [umfangreiches Vortrags- und
Workshopprogramm](/programm/). Tage ohne besonderes Programm konnte mensch an
einer Hand abzählen. Besonders wichtig war es uns, Stimmen aus der Wissenschaft
Gehör zu verschaffen und damit uns sowie die stets eingeladene Lokalpolitik
weiterzubilden. Als die Corona-Inzidenz so besorgniserregend zunahm, pausierten
wir unser Vortragsprogramm.

Jeden Tag sprechen wir mit Passant\*innen, die am Camp vorbeikommen oder uns
besuchen; wir verlesen Reden; und wir machen mit Sprechchören und Liedern auf das
klimapolitische Totalversagen der Regierung aufmerksam. Unsere
Öffentlichkeitsarbeit steht auch im Fokus unseres
[Video-Adventskalenders](/adventskalender/) und unseres [Podcasts](/podcast/).

Daneben führen wir zahlreiche Gespräche mit Lokalpolitiker\*innen -- im Sommer
auch oft öffentlich in Form von Podiumsdiskussionen. Dabei erlangten wir den
Eindruck, dass die Opposition im Augsburger Stadtrat die nötige Eile in der
Bekämpfung der Klimakrise weitgehend verstanden hat, während die schwarz-grüne
Regierungskoalition weiterhin das Wohlergehen der Wirtschaft im kommenden
Quartal über die Bedürfnisse der Menschen (und der Wirtschaft in langfristiger
Perspektive) stellt.

Daher führen wir [zahlreiche Aktionen](/pressemitteilungen/) durch, die im
Klimacamp ihren Anfang nehmen.


## Unterstützung

Du möchtest das Klimacamp aktiv unterstützen, weißt aber nicht wie?
Dann komm gerne in unsere [Unterstützungsgruppe auf
Telegram](https://t.me/joinchat/GebYOlhhZMoAtCQQ).

Wenn es eine Aufgabe gibt, die zu erledigen ist, wird sie in diese Gruppe
gestellt. Wenn du die Aufgabe dann übernehmen möchtest kannst du dich dann per
Direktnachricht melden. Wenn du ein Angebot hast, wie du uns unterstützen
möchtest, kannst du das auch gerne in den Chat oder als Privatnachicht
schreiben.


## Corona

Dadurch, dass wir im Freien sind und aufgrund der
besonderen Lage zwischen Ober- und Unterstadt fast immer ein Wind weht, ist das
Infektionsrisiko auf dem Camp zum Glück reduziert. Trotzdem nehmen wir seit
Beginn des Camps die Corona-Pandemie sehr ernst. Schon seit Tag 1 des Camps gilt: Wir halten Abstand
und tragen Maske. Desinfektionsmittelfläschchen sind über das ganze Campgelände
verteilt, sodass sie stets griffbereit sind. Im Hinblick der besorgniserregenden
aktuellen Entwicklung der Situation erhalten alle Menschen, die sich auf dem
Camp aufhalten, eine Belehrung über unser Schutzkonzept.

**Aktuelle Regeln (20.12.)**: Im schlecht durchlüfteten großen hinteren Zelt
darf sich stets nur eine Corona-Bezugsgruppe aufhalten, im Haus vorne und im
Küchenpavillon nur zwei Personen. Der Küchenpavillon darf nur mit desinfizierten Händen betreten
werden. Vorträge mit großem Publikum sollen momentan nicht im Camp stattfinden.
Wir halten stets echte 1½ Meter Abstand, beim Essen echte 3 Meter. Unsere Maske
nehmen wir nur zum Essen ab.


## Impressum

Direkter Kontakt zur rechtlich verantwortlichen Person: +49 176 95110311 (Ingo Blechschmidt, Arberstr. 5, 86179 Augsburg, iblech@web.de).
Weiterer Kontakt:
+49 177 3639551 (Paula Stoffels, paula.stoffels@web.de).

<script data-no-instant>
  {% include instantclick.min.js %}
  InstantClick.init();
</script>

<img src="https://www.speicherleck.de/klimacamp-footer.jpeg" style="width: 0; height: 0">

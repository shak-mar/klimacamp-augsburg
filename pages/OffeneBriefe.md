---
layout: page
title: Offene Briefe
permalink: /offeneBriefe/
has_children: true
nav_order: 6
---

# Offene Briefe

Hier gibt es eine Auflistung von offenen Briefen, die das Klimacamp an Personen, Gruppen von Personen oder Organisationen verfasst hat.

---
layout: page
title: Podcast
permalink: /podcast/
nav_order: 11
---

<div style="float: right; padding-left: 1em">
  {% include podlove-button.html %}
</div>

# Der Podcast des Klimacamps

Wir bleiben bis ihr handelt! Berichte aus dem und über das Augsburger Klimacamp.

* [YouTube](https://www.youtube.com/watch?v=BdUBZEoE2SI&list=PLo149GNLBLWCSd9BbrIBS3sjvs3NxbfzV)
* [Spotify](https://open.spotify.com/show/4IUA2iZtNQI6eermX8P1cO)
* [iTunes](https://podcasts.apple.com/de/podcast/klimacamp-augsburg-podcast/id1545035012)
* [fyyd](https://fyyd.de/podcast/klimacamp-augsburg-podcast/0)

Oder [direkt zum Feed](/podcast.xml).

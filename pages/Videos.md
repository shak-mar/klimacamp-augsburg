---
layout: page
title: Videos zur Klimagerechtigkeit
permalink: /videos/
nav_exclude: true
---

# Videos zur Klimagerechtigkeit

Hier werden Vorträge und ähnliche Videos verlinkt zu verschiedenen Themen. Die jeweils für uns wichtigsten Videos zu den verschiedenen Themen werden hier mit Vorschaubild angezeigt. Wenn Sie sich einen informierten, groben Überblick verschaffen wollen, sind diese völlig ausreichend für Sie. Wenn Sie sich weiter informieren wollen, können Sie die Links darunter ohne Bild ansehen.


## Allgemeines, Einführendes

+ [Klimaschutz in der Sackgasse](https://www.youtube.com/watch?v=z5mdsUgsI3M)
    + Vortrag von Prof. Volker Quaschning
    + Sprache: Deutsch
    + ein leicht verständlicher, stellenweise unterhaltsamer Vortrag, der viele Bereiche des Klimas anschneidet, mit leichtem Fokus auf dem Thema Energiewende

+ [Climate Change Is An Absolute Nightmare - This is Why](https://www.youtube.com/watch?v=uqwvf6R1_QY)
    + YouTube-Video von (Chemiker) UpIsNotJump
    + Sprache: Englisch
    + enthält überraschend viele einleuchtende und einprägsame wissenschaftliche Fakten zum Klimawandel - verständlich und unterhaltsam erklärt
    + Quellen und Weiterführendes in der Videobeschreibung


## Energie

+ [100% Erneuerbare Energie - (Wie) Geht das? || Klimacamp Augsburg](https://www.youtube.com/watch?v=AxjU6MzrbS8&t=3142s)
    + Vortrag von Dr. Peter Klafka
    + Sprache: Deutsch


Bei Gelegenheit stellen wir hier noch weitere Videos ein. Vorschläge gerne an iblech@web.de, im Betreff bitte folgende Schlagworte erwähnen: Videos Klimacamp Website :-)

<!-- ## Mobilität


### Fußgehen


### ÖPNV (Bus & Tram)
Der öffentliche Personennahverkehr

### Fahrrad


### Fernverkehr (<- besseren Namen?)
Als letzter Teil ergänzen die Zugstrecken außerhalb unserer Innenstädte den Umweltverbund (Fuß, Rad, Schiene). ...


### (E-)Auto

Quaschning Videos zu E-Autos -->

---
layout: page
title: Spenden
permalink: /spenden/
nav_order: 130
---

# Spenden

Wir sind über Spenden sehr dankbar und wissen diese sehr zu schätzen.

Wir finanzieren darüber Campmaterialien, Flyer, Plakate, Banner, gelegentlich
Klettermaterialien, juristische Ausgaben und diverse kleinere Posten.

Dankenswerterweise nimmt die [Bürgerstiftung
Augsburg](https://www.buergerstiftung-augsburg.de/) für uns Spenden entgegen.
Die Kontodaten lauten:

    Name: Bürgerstiftung Augsburg
    IBAN: DE22 7205 0000 0000 0263 69
    Verwendungszweck: Klimacamp (unbedingt angeben, sonst kann die Spende nicht zugeordnet werden!)

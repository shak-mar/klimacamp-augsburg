---
layout: page
title: Pressespiegel
permalink: /pressespiegel/
nav_order: 8
---

# Pressespiegel
Das sagen die Anderen über uns (unvollständig):

## 13.5.
* Augsburger Allgemeine: [Meitinger Klima-Aktivisten bleiben diesmal mit beiden Beinen am Boden](https://www.augsburger-allgemeine.de/augsburg-land/Meitingen-Meitinger-Klima-Aktivisten-bleiben-diesmal-mit-beiden-Beinen-am-Boden-id59668431.html)
* Forum solidarisches und friedliches Augsburg: [Augsburger Ostermarsch, Teil 2](https://www.forumaugsburg.de/s_3themen/Antimil/210512_ostermarsch-2021_2/index.htm)

## 12.5.
* Bayerischer Rundfunk: [Augsburgs Stadtrat diskutiert Klimaschutzprojekte](https://www.br.de/nachrichten/bayern/augsburgs-stadtrat-diskutiert-klimaschutzprojekte,SXCVT3P)

## 11.5.
* Augsburger Allgemeine: [Klimaaktivisten blockieren Gögginger Straße vor Stadtratssitzung in Augsburg](https://www.augsburger-allgemeine.de/augsburg/Augsburg-Klimaaktivisten-blockieren-Goegginger-Strasse-vor-Stadtratssitzung-in-Augsburg-id59670526.html)
* Augsburger Allgemeine: [Augsburg muss für die Klimawende den Verkehr massiv umkrempeln](https://www.augsburger-allgemeine.de/augsburg/Augsburg-Augsburg-muss-fuer-die-Klimawende-den-Verkehr-massiv-umkrempeln-id59673876.html)

## 8.5.
* a.tv: [Augsburger Sonder-Stadtrat für mehr Klimaschutz](https://www.augsburg.tv/mediathek/video/augsburger-sonder-stadtrat-fuer-mehr-klimaschutz/)

## 3.5.
* Radio Augsburg: [Klima-Aktivisten setzen auf Schaukeln](https://radioaugsburg.de/klima-aktivisten-setzen-auf-schaukeln/)

## 24.4.
* Augsburger Allgemeine: [Meitinger Aktivisten wollen nicht mit Mehring ü?ber Lohwald reden](https://www.augsburger-allgemeine.de/augsburg-land/Meitinger-Aktivisten-wollen-nicht-mit-Mehring-ueber-Lohwald-reden-id59553261.html)

## 16.4.
* Augsburger Allgemeine: [Analyse von Wetterdaten: So zeigt sich in Augsburg schon jetzt der Klimawandel](https://www.augsburger-allgemeine.de/augsburg/Analyse-von-Wetterdaten-So-zeigt-sich-in-Augsburg-schon-jetzt-der-Klimawandel-id59499281.html)
* Augsburger Allgemeine: [Die strengen Klimaziele sind richtig](https://www.augsburger-allgemeine.de/augsburg/Die-strengen-Klimaziele-der-Stadt-Augsburg-sind-richtig-id59504461.html)

## 15.4.
* Augsburger Allgemeine: [Warum Klima-Aktivisten Fabian Mehring ein Hochbeet schenken](https://www.augsburger-allgemeine.de/augsburg-land/Warum-Klima-Aktivisten-Fabian-Mehring-ein-Hochbeet-schenken-id59492496.html)

## 13.4.
* Augsburger Allgemeine: [So bereiten sich die Klima-Aktivisten auf Aktionen im Meitinger Lohwald vor](https://www.augsburger-allgemeine.de/augsburg-land/So-bereiten-sich-die-Klima-Aktivisten-auf-Aktionen-im-Meitinger-Lohwald-vor-id59477346.html)

## 11.4.
* Augsburger Allgemeine: [Aktivisten setzen erneut Zeichen gegen Lohwald-Rodung](https://www.augsburger-allgemeine.de/augsburg-land/Aktivisten-setzen-erneut-Zeichen-gegen-Lohwald-Rodung-id59472581.html)

## 10.4.
* Forum solidarisches und friedliches Augsburg: [700 beim Globalen Klimastreik auf dem Plärrer](https://www.forumaugsburg.de/s_2kommunal/Kommunalreform/210410_globaler-klimastreik-fridays-for-future-auf-dem-plaerrer/index.htm)

## 9.4.
* Augsburger Allgemeine: [Klima-Aktivisten hängen Banner zum Schutz des Lohwalds auf](https://www.augsburger-allgemeine.de/augsburg-land/Klima-Aktivisten-haengen-Banner-zum-Schutz-des-Lohwalds-auf-id59455471-amp.html)
* Augsburger Allgemeine: [Proteste wie im Hambacher Forst? "Wenn es sein muss, besetzen wir den Wald"](https://www.augsburger-allgemeine.de/augsburg-land/Proteste-wie-im-Hambacher-Forst-Wenn-es-sein-muss-besetzen-wir-den-Wald-id59462481.html)

## 28.3.
* Augsburger Allgemeine: [Umwelt & Co.: Menschen protestieren in Augsburg gegen Missstände](https://www.augsburger-allgemeine.de/augsburg/Umwelt-Co-Menschen-protestieren-in-Augsburg-gegen-Missstaende-id59395086.html)

## 27.3.
* Augsburger Allgemeine: [Nicht nur wegen Corona: Zahl der Demos in Augsburg schießt nach oben](https://www.augsburger-allgemeine.de/augsburg/Nicht-nur-wegen-Corona-Zahl-der-Demos-in-Augsburg-schiesst-nach-oben-id59361536.html)

## 23.3.
* Augsburger Allgemeine: [Stadtwerke Augsburg wollen Haushalte nur noch mit Ökostrom beliefern](https://www.augsburger-allgemeine.de/augsburg/Stadtwerke-Augsburg-wollen-Haushalte-nur-noch-mit-Oekostrom-beliefern-id59358186.html)

## 19.3.
* Augsburger Allgemeine: [Fridays for Future: Rund 500 Menschen demonstrieren in Augsburg für das Klima](https://www.augsburger-allgemeine.de/augsburg/Fridays-for-Future-Rund-500-Menschen-demonstrieren-in-Augsburg-fuer-das-Klima-id59345631.html)
* Augsburger Allgemeine: [Augsburger Aktivisten von Fridays for Future demonstrieren am Plärrer](https://www.augsburger-allgemeine.de/augsburg/Augsburger-Aktivisten-von-Fridays-for-Future-demonstrieren-am-Plaerrer-id59341776.html)

## 13.3.
* Salzburger Nachrichten: [Campen fürs Klima: Wie eine Stadt in Bayern den Klimaprotest weiterführt](https://www.sn.at/panorama/klimawandel/campen-fuers-klima-wie-eine-stadt-in-bayern-den-klimaprotest-weiterfuehrt-100891069)
* Augsburger Allgemeine: [Klimacamp-Aktivisten protestieren gegen Abriss der "Diesel-Villa"](https://www.augsburger-allgemeine.de/augsburg/Klimacamp-Aktivisten-protestieren-gegen-Abriss-der-Diesel-Villa-id59301026.html)

## 10.3.
* Augsburger Allgemeine: [Augsburg will mehr Platz für Radler schaffen -- und damit weniger für Autos](https://www.augsburger-allgemeine.de/augsburg/Augsburg-will-mehr-Platz-fuer-Radler-schaffen-und-damit-weniger-fuer-Autos-id59271501.html)
* Augsburger Allgemeine: [Die Fahrradstadt Augsburg kommt nur langsam ins Rollen](https://www.augsburger-allgemeine.de/augsburg/Die-Fahrradstadt-Augsburg-kommt-nur-langsam-ins-Rollen-id59272996.html)

## 8.3.
* a.tv: [Reportage: Ein Tag im Klimacamp](https://www.augsburg.tv/mediathek/video/reportage-ein-tag-im-klimacamp/)

## 5.3.
* Augsburger Allgemeine: [Tarifreform im AVV: Wo bleibt die Sicht der Fahrgäste?](https://www.augsburger-allgemeine.de/augsburg/Tarifreform-im-AVV-Wo-bleibt-die-Sicht-der-Fahrgaeste-id59244101.html)

## 3.3.
* Augsburger Allgemeine: [Gibt es beim Fahrrad-Bürgerbegehren jetzt eine Einigung mit der Stadt?](https://www.augsburger-allgemeine.de/augsburg/Gibt-es-beim-Fahrrad-Buergerbegehren-jetzt-eine-Einigung-mit-der-Stadt-id59225031.html)
* Augsburger Allgemeine: [Fahrrad-Bürgerbegehren: Verhandlungspartner stehen unter Druck](https://www.augsburger-allgemeine.de/augsburg/Fahrrad-Buergerbegehren-Verhandlungspartner-stehen-unter-Druck-id59225821.html)

## 28.2.
* Bayerischer Rundfunk: [Bannwaldrodung für Lech-Stahlwerke: Umweltschützer demonstrieren](https://www.br.de/nachrichten/bayern/bannwaldrodung-fuer-lech-stahlwerke-umweltschuetzer-demonstrieren,SQK6YTp)
* Augsburger Allgemeine: [Hunderte protestieren lautstark für den Erhalt des Lohwalds bei Meitingen](https://www.augsburger-allgemeine.de/augsburg-land/Hunderte-protestieren-lautstark-fuer-den-Erhalt-des-Lohwalds-bei-Meitingen-id59211951.html)
* Augsburger Allgemeine: [Ein deutliches Zeichen für den Erhalt des Lohwalds](https://www.augsburger-allgemeine.de/augsburg-land/Ein-deutliches-Zeichen-fuer-den-Erhalt-des-Lohwalds-id59212741.html)
* Presse Augsburg: [600 Teilnehmer demonstrieren in Meitingen gegen Rodung des Bannwaldes](https://presse-augsburg.de/600-teilnehmer-demonstrieren-in-meitingen-gegen-rodung-des-bannwaldes/701998/)

## 26.2.
* Augsburger Allgemeine: [Augsburger Stadtrat plant Sondersitzung zum Thema Klima](https://www.augsburger-allgemeine.de/augsburg/Augsburger-Stadtrat-plant-Sondersitzung-zum-Thema-Klima-id59195866.html)

## 23.2.
* Augsburger Allgemeine: [Demonstration für den Erhalt des Bannwalds bei Meitingen](https://www.augsburger-allgemeine.de/augsburg-land/Demonstration-fuer-den-Erhalt-des-Bannwalds-bei-Meitingen-id59180566.html)

## 21.2.
* Neue Sonntagspresse: [Durchhalten im Klima-Camp -- egal wie das Wetter ist](https://neuesonntagspresse.de/images/ausgaben/2021/21-02-21.pdf#page=3)

## 13.2.
* Augsburger Allgemeine: Wie viel Kommunikation kann sich die Stadt leisten?

## 10.2.
* ZDF logo: [Augsburg: Jugendliche campen für's Klima](https://www.zdf.de/kinder/logo/fridays-for-future-corona-camp-100.html)

## 7.2.
* BR Schwaben & Altbayern: [Erfolgreicher Dauerprotest -- Das Klimacamp Augsburg](https://www.br.de/mediathek/video/erfolgreicher-dauerprotest-das-klimacamp-augsburg-av:601ef0efdaef33001a2d2d31)

## 5.2.
* Augsburger Allgemeine: [Klimaaktivisten wollen mit spektakulärer Aktion Rodung im Stadtwald verhindern](https://www.augsburger-allgemeine.de/augsburg/Klimaaktivisten-wollen-mit-spektakulaerer-Aktion-Rodung-im-Stadtwald-verhindern-id59048881.html)
* Augsburger Allgemeine: [Klimaaktivisten warnen im Augsburger Stadtwald vor Rodungen](https://www.youtube.com/watch?v=n1aOVcHDuXA) (Video auf YouTube)

## 3.2.
* Augsburger Allgemeine: Auf dem Gersthofer Festplatz kehrt Ruhe ein

## 1.2.
* Augsburger Allgemeine: [Klima-Aktivisten kämpfen um Bäume am Gersthofer Festplatz](https://www.augsburger-allgemeine.de/augsburg-land/Klima-Aktivisten-kaempfen-um-Baeume-am-Gersthofer-Festplatz-id59011896.html)
* Augsburger Allgemeine: [Klima-Aktivisten können Baumfällung in Gersthofen nur aufschieben](https://www.augsburger-allgemeine.de/augsburg-land/Klima-Akivisten-koennen-Baumfaellung-in-Gersthofen-nur-aufschieben-id59016136.html)
* a.tv: [a.tv Aktuell](https://www.augsburg.tv/mediathek/video/a-tv-aktuell-vom-1-02-2021/) (Zeitstempel 12:36)

## 30.1.
- Augsburger Allgemeine: [Autoverkehr: Grenzen des Wachstums in Augsburg sind erreicht](https://www.augsburger-allgemeine.de/augsburg/Autoverkehr-Grenzen-des-Wachstums-in-Augsburg-sind-erreicht-id59002156.html)

## 29.1.
- ARD Tagesthemen: [Klimacamp in Augsburg](https://youtu.be/94LAR0I_BSY?t=810)

## 26.1.
- Augsburger Allgemeine: [CO2-Ausstoß: Klimacamp begrüßt Klimabeschluss der Stadt Augsburg](https://augsburger-allgemeine.de/augsburg/CO2-Ausstoss-Klimacamp-begruesst-Klimabeschluss-der-Stadt-Augsburg-id58976001.html)

## 22.1.
- Augsburger Allgemeine: [Augsburgs Umweltreferent Reiner Erben kontert Kritik der Klimaaktivisten](https://www.augsburger-allgemeine.de/augsburg/Augsburgs-Umweltreferent-Reiner-Erben-kontert-Kritik-der-Klimaaktivisten-id58951851.html)
- a.tv: [Augsburgs Umweltreferent wehrt sich gegen Vorwürfe](https://www.augsburg.tv/mediathek/video/augsburgs-umweltreferent-wehrt-sich-gegen-vorwuerfe/)

## 21.1.
- Augsburger Allgemeine: [Klimaschutz: Augsburg will mit neuen Maßnahmen mehr CO2 sparen](https://www.augsburger-allgemeine.de/augsburg/Klimaschutz-Augsburg-will-mit-neuen-Massnahmen-mehr-CO2-sparen-id58944391.html)
- Augsburger Allgemeine: [CO2-Einsparungen sind in Augsburg überfällig](https://www.augsburger-allgemeine.de/augsburg/CO2-Einsparungen-sind-in-Augsburg-ueberfaellig-id58946001.html)

## 2.1.
- Augsburger Allgemeine: [Augsburger Klimacamp will Protest ausweiten: "Sprösslingscamps" geplant](https://www.augsburger-allgemeine.de/augsburg/Augsburger-Klimacamp-will-Protest-ausweiten-Sproesslingscamps-geplant-id58838906.html)
- Bayerischer Rundfunk: [Bilanz nach einem halben Jahr](https://www.br.de/mediathek/video/klimacamp-augsburg-bilanz-nach-einem-halben-jahr-av:5ff0918adf6f39001a195b4c)
- Bayerischer Rundfunk: [Augsburger Klimacamp will Druck auf Politik erhöhen](https://www.br.de/nachrichten/bayern/augsburger-klimacamp-will-druck-auf-politik-erhoehen,SKqE68b)

## 31.12.
- Augsburger Allgemeine: [Silvester 2020: Eine stille Nacht zum Jahreswechsel in Augsburg](https://m.augsburger-allgemeine.de/augsburg/Silvester-2020-Eine-stille-Nacht-zum-Jahreswechsel-in-Augsburg-id58834246.html)

## 18.12.
- Augsburger Allgemeine: [Weniger CO2-Ausstoß: Augsburg will Druck beim Klimaschutz machen](https://augsburger-allgemeine.de/augsburg/Weniger-CO2-Ausstoss-Augsburg-will-Druck-beim-Klimaschutz-machen-id58754701.html)

## 17.12.
- Bayerischer Rundfunk: [Augsburger Stadtrat beschließt Klimaschutz-Sofortprogramm](https://www.br.de/nachrichten/bayern/klimaschutz-sofortprogramm-fuer-augsburg-stadtrat-stimmt-ab,SJPwNR2)

## 16.12.
- Allgäuer Zeitung: [Zoff ums Klimacamp vor dem Rathaus - Augsburg klagt gegen junge Klimaschützer](https://www.allgaeuer-zeitung.de/bayern/streit-der-stadt-augsburg-mit-klimasch%C3%BCtzern-eskaliert_arid-255652)
- TAG24: ["Das ist kein Protest, das ist Provokation": Klimacamp von "Fridays for Future" sorgt für Wirbel](https://www.tag24.de/nachrichten/politik/fridays-for-future/klimacamp-von-fridays-for-future-vor-dem-rathaus-sorgt-fuer-wirbel-in-augsburg-1763320)
- GMX.de: [Augsburg und Klimaschützer bleiben auf Konfrontationskurs](https://www.gmx.net/magazine/regio/bayern/augsburg-klimaschuetzer-konfrontationskurs-35358310)

## 15.12.
- Augsburger Allgemeine: [Klimacampern reicht das Beschlusspaket der Stadt nicht](https://www.augsburger-allgemeine.de/augsburg/Klimacampern-reicht-das-Beschlusspaket-der-Stadt-nicht-id58734216.html)
- Bayerischer Rundfunk: [Rückblick - Das Jahr 2020 in Schwaben](https://www.br.de/nachrichten/bayern/rueckblick-das-jahr-2020-in-schwaben,SIg2ifq)

## 14.12.
- Augsburger Allgemeine: [Stadt Augsburg will sich beim Klimaschutz mehr Druck machen](https://augsburger-allgemeine.de/augsburg/Stadt-Augsburg-will-sich-beim-Klimaschutz-mehr-Druck-machen-id58721061.html)
- Augsburger Allgemeine: [Was sagen die Klimacamper zum Klimaschutz-Paket der Stadt?](https://www.augsburger-allgemeine.de/augsburg/Was-sagen-die-Klimacamper-zum-Klimaschutz-Paket-der-Stadt-id58730491.html)

## 13.12.
- Augsburger Allgemeine: [Debatte: Die Stadt Augsburg muss beim Klimaschutz Farbe bekennen](https://www.augsburger-allgemeine.de/augsburg/Debatte-Die-Stadt-Augsburg-muss-beim-Klimaschutz-Farbe-bekennen-id58710141.html)

## 12.12.
- stayfm: [doppelstunde klima -- miri und die schweigende mehrheit](https://hearthis.at/stayfm/doppelstunde-klima-vom-12-12-20/)

## 10.12.
- Augsburger Allgemeine: [Klimacamp-Aktivisten bereiten sich auf Winter am Rathaus vor](https://augsburger-allgemeine.de/augsburg/Klimacamp-Aktivisten-bereiten-sich-auf-Winter-am-Rathaus-vor-id58696186.html)
- Augsburger Allgemeine: [Klimacamp: Hut ab vor dem Engagement](https://augsburger-allgemeine.de/augsburg/Klimacamp-Hut-ab-vor-dem-Engagement-id58699526.html)
- Augsburger Allgemeine: [Kreide-Schriftzug am Rathausplatz löst umstrittenen Polizeieinsatz aus](https://www.augsburger-allgemeine.de/augsburg/Kreide-Schriftzug-am-Rathausplatz-loest-umstrittenen-Polizeieinsatz-aus-id58705391.html)
- Die Augsburger Zeitung: [Kommentar zur Catcalls-of-Augsburg-Aktion: Agiert die Augsburger Polizei politisch?](https://www.daz-augsburg.de/kommentar-zur-catcall-of-augsburg-aktion-agiert-die-augsburger-polizei-politisch/)
- AUXPUNKS: [Du widerliches Stück Augsburg!](https://auxpunks.uber.space/du-widerliches-stueck-augsburg/)

## 9.12.
- STADTGRÜN: [Unermüdlich fürs Klima](https://gruene-augsburg.de/userspace/BY/sv_augsburg/Dokumente/STADTGRUEN/20201211_Infobrief_9_2020_final.pdf#page=19)

## 7.12.
- Hallo Augsburg: [Lokale Agenda 21: Klimacamp ist Aufruf und Erinnerung](https://www.hallo-augsburg.de/klimacamp-augsburg-lokale-agenda-21-klimacamp-ist-aufruf-und-erinnerung_mKq)

## 6.12.
- Forum solidarisches und friedliches Augsburg: [Das Klimacamp feiert sein 100-tägiges Jubiläum. Debatte mit Stadträt_innen, Teil 2](https://www.forumaugsburg.de/s_2kommunal/Kommunalreform/201205_100-tage-klimacamp-diskussionsrunde-mit-den-stadtraet_innen-2/index.htm)
- Die Augsburger Zeitung: [Kommentar zum Radentscheid: Es gibt nichts zu verhandeln!](https://www.daz-augsburg.de/kommentar-zum-radentscheid-es-gibt-nichts-zu-verhandeln/)

## 4.12.
- Augsburger Allgemeine: [Fahrrad-Bürgerbegehren: Rad-Aktivisten machen Druck auf die Stadt Augsburg](https://m.augsburger-allgemeine.de/augsburg/Fahrrad-Buergerbegehren-Rad-Aktivisten-machen-Druck-auf-die-Stadt-Augsburg-id58665526.html)
- Lifeguide Augsburg: [Klimacamp Augsburg soll bleiben!](https://www.lifeguide-augsburg.de/magazin/klimacamp-augsburg-soll-bleiben)

## 2.12.
- Süddeutsche Zeitung: [Frieren für das Klima](https://www.sueddeutsche.de/bayern/augsburg-klima-protest-fridays-for-future-1.5134645?reduced=true)
- Augsburger Hochschulmagazin presstige: [„So ein bisschen hatte ich schon immer das Gefühl, dass man da eigentlich was machen müsste“](http://presstige.org/2020/12/interview-klimacamper/)

## 29.11.
- Augsburger Allgemeine: [Das Augsburger Klimacamp lässt keinen kalt – und das ist gut so](https://augsburger-allgemeine.de/augsburg/Das-Augsburger-Klimacamp-laesst-keinen-kalt-und-das-ist-gut-so-id58625521.html)

## 27.11.
- Augsburger Allgemeine: [Geplante Räumung des Klimacamps: Sozialfraktion greift Stadt Augsburg an](https://augsburger-allgemeine.de/augsburg/Geplante-Raeumung-des-Klimacamps-Sozialfraktion-greift-Stadt-Augsburg-an-id58622056.html)

## 26.11.
- Bayerischer Rundfunk: [Klimacamper in Augsburg: "CSU will uns unbedingt weghaben"](https://www.br.de/nachrichten/bayern/klimacamper-in-augsburg-csu-will-uns-unbedingt-weghaben,SHQOFru)

## 25.11.
- Augsburger Allgemeine: [Die Stadt Augsburg will das Klimacamp weiterhin loswerden](https://www.augsburger-allgemeine.de/augsburg/Die-Stadt-Augsburg-will-das-Klimacamp-weiterhin-loswerden-id58610141.html)
- Presse Augsburg: [Klimacamp: Stadt Augsburg beantragt Zulassung von Berufung gegen VWG-Urteil](https://presse-augsburg.de/klimacamp-stadt-augsburg-beantragt-zulassung-von-berufung-gegen-vwg-urteil/668067/)
- Bayerischer Rundfunk: [Klimacamp: Stadt zieht vor den Verwaltungsgerichtshof](https://www.br.de/nachrichten/bayern/klimacamp-stadt-zieht-vor-den-verwaltungsgerichtshof,SHNHEQa)
- Stadtzeitung: [Stadt Augsburg will weiter gegen das Klimacamp vorgehen](https://www.stadtzeitung.de/region/augsburg/politik/stadt-augsburg-will-gegen-klimacamp-vorgehen-id215756.html)
- Die Augsburger Zeitung: [Klimacamp: Stadt geht gegen das Urteil des Augsburger Verwaltungsgerichts in Berufung — Grüne kritisieren Vorgehen der Stadt](https://www.daz-augsburg.de/klimacamp-stadt-geht-gegen-das-urteil-des-augsburger-verwaltungsgerichts-in-berufung-gruene-kritisieren-dieses-vorgehen/)

## 24.11.
- Forum solidarisches und friedliches Augsburg: [Das Verwaltungsgericht Augsburg entscheidet im Rechtsstreit pro Klimacamp, Teil 2](https://www.forumaugsburg.de/s_2kommunal/Kommunalreform/201124_verwaltungsgericht-entscheidet-pro-klimacamp-2-folgen-fuer-bayern/index.htm)

## 23.11.
- Die Augsburger Zeitung: [Zukunftspreis: CSU-Chef Ullrich und Fridays for Future-Bewegung kritisieren Stadt wegen AfD-Stadtrat in Jury](https://www.daz-augsburg.de/zukunftspreis-csu-chef-ullrich-und-fridays-for-future-bewegung-kritisieren-stadt-wegen-afd-stadtrat-in-jury/)

## 20.11.
- Junge Welt: [»Wir schlafen nicht in der Kälte, weil es uns Spaß macht«](https://www.jungewelt.de/artikel/390874.klimacamp-in-augsburg-wir-schlafen-nicht-in-der-k%C3%A4lte-weil-es-uns-spa%C3%9F-macht.html)

## 19.11.
- Forum solidarisches und friedliches Augsburg: [Eine schwere Niederlage für die Stadtverwaltung und die CSU](https://www.forumaugsburg.de/s_2kommunal/Kommunalreform/201119_verwaltungsgericht-entscheidet-pro-klimacamp/index.htm)

## 14.11.
- Augsburger Allgemeine (Lokalteil Friedberg): [Osttangenten-Gegner erhalten Unterstützung vom Augsburger Klimacamp](https://m.augsburger-allgemeine.de/friedberg/Osttangenten-Gegner-erhalten-Unterstuetzung-vom-Augsburger-Klimacamp-id58528891.html)
- Bayerischer Rundfunk: [Fridays-for-Future-Aktivisten erhalten Augsburger Zukunftspreis](https://www.br.de/nachrichten/bayern/augsburger-klimaaktivisten-erhalten-zukunftspreis-der-stadt,SGJM02T)

## 13.11.
- Augsburger Allgemeine: [Soll das Klimacamp weichen? Grüne lehnen weiteren Rechtsstreit ab](https://m.augsburger-allgemeine.de/augsburg/Soll-das-Klimacamp-weichen-Gruene-lehnen-weiteren-Rechtsstreit-ab-id58531696.html)
- Augsburger Allgemeine: [Klima-Aktivisten erhalten Augsburger Zukunftspreis](https://www.augsburger-allgemeine.de/augsburg/Klima-Aktivisten-erhalten-den-Augsburger-Zukunftspreis-id58538326.html)

## 11.11.
- Die Augsburger Zeitung: [Klimapolitik: Stadt will CO2-Emissionen bis zum Jahr 2030 halbieren](https://www.daz-augsburg.de/klimapolitik-stadt-will-co2-emissionen-bis-zum-jahr-2030-halbieren/)
- Die Augsburger Zeitung: [“Das Urteil verschafft uns zusätzliche Legitimität” – Interview mit Klimacamper Ingo Blechschmidt](https://www.daz-augsburg.de/das-urteil-verschafft-uns-zusaetzliche-legitimitaet-interview-mit-klimacamper-ingo-blechschmidt/)
- a.tv: [Klimacamp darf bleiben: Reaktion der Stadt Augsburg](https://www.augsburg.tv/mediathek/video/klimacamp-darf-bleiben-reaktionen-der-stadt-augsburg/)

## 10.11.
- Augsburger Allgemeine: [Das Klimacamp neben dem Augsburger Rathaus darf bleiben](https://www.augsburger-allgemeine.de/augsburg/Das-Klimacamp-neben-dem-Augsburger-Rathaus-darf-bleiben-id58514851.html)
- Augsburger Allgemeine: [Klimacamp: Augsburg hat sich vergaloppiert](https://m.augsburger-allgemeine.de/augsburg/Klimacamp-Augsburg-hat-sich-vergaloppiert-id58517731.html)
- Augsburger Allgemeine: [Experte: So ungleich trifft der Klimawandel die Menschen in Augsburg](https://m.augsburger-allgemeine.de/augsburg/Experte-So-ungleich-trifft-der-Klimawandel-die-Menschen-in-Augsburg-id58473626.html)
- Stadtzeitung: [Gericht entscheidet: Augsburger Klimacamp darf bleiben](https://www.stadtzeitung.de/region/augsburg/politik/gericht-entscheidet-augsburger-klimacamp-darf-bleiben-id215108.html)
- Bayerischer Rundfunk: [Verwaltungsgericht Augsburg: Klimacamp darf endgültig bleiben](https://www.br.de/nachrichten/bayern/verwaltungsgericht-augsburg-klimacamp-darf-endgueltig-bleiben,SFwjPny)
- Süddeutsche Zeitung: [Aktivisten dürfen weiter neben Rathaus campen](https://www.sueddeutsche.de/bayern/augsburg-klimacamp-raeumung-urteil-1.5110839I)
- Schwäbische: [Stadt Augsburg muss Klimacamp neben Rathaus hinnehmen](https://www.schwaebische.de/sueden/bayern_artikel,-stadt-augsburg-muss-klimacamp-neben-rathaus-hinnehmen-_arid,11292737.html)
- Zeit: [Stadt Augsburg muss Klimacamp neben Rathaus hinnehmen](zeit.de/news/2020-11/10/stadt-augsburg-muss-klimacamp-neben-rathaus-hinnehmen)
- a.tv: [Klimacamp darf bleiben](https://www.augsburg.tv/mediathek/video/a-tv-kompakt-klimacamp-darf-bleiben/)
- Traunsteiner Tagblatt: [Stadt Augsburg muss Klimacamp neben Rathaus hinnehmen](https://www.traunsteiner-tagblatt.de/region/nachrichten-aus-bayern_artikel,-stadt-augsburg-muss-klimacamp-neben-rathaus-hinnehmen-_arid,599449.html)
- Donaukurier: [Stadt Augsburg muss Klimacamp neben Rathaus hinnehmen](https://www.donaukurier.de/nachrichten/bayern/Demonstrationen-Umwelt-Prozesse-Urteile-SCHWABEN-Bayern-Deutschland-Stadt-Augsburg-muss-Klimacamp-neben-Rathaus-hinnehmen;art155371,4713096)
- Presse Augsburg: [Verwaltungsgericht erklärt „Klimacamp“ am Augsburger Rathaus zu einer Versammlung im rechtlichen Sinne](https://presse-augsburg.de/verwaltungsgericht-erklaert-klimacamp-am-augsburger-rathaus-zu-einer-versammlung-im-rechtlichen-sinne/657077/)
- Frankenpost: [Stadt Augsburg muss Klimacamp neben Rathaus hinnehmen](https://www.frankenpost.de/region/bayern/Stadt-Augsburg-muss-Klimacamp-neben-Rathaus-hinnehmen;art2832,7462672)
- Radio Fantasy: [Augsburger Klimacamp darf vorerst nicht geräumt werden](https://www.fantasy.de/lokalnews/10296-augsburger-klimacamp-darf-vorerst-nicht-geraeumt-werden)
- ntv: [Stadt Augsburg muss Klimacamp neben Rathaus hinnehmen](https://www.n-tv.de/regionales/bayern/Stadt-Augsburg-muss-Klimacamp-neben-Rathaus-hinnehmen-article22160685.html)

## 7.11.
- Aktionsbündnis keine Osttangente: [Klimacamp und AKO – gemeinsam gegen den Klimawandel](https://keine-osttangente.de/?p=2042)
- Forum solidarisches und friedliches Augsburg: [Das Klimacamp feiert sein 100-tägiges Jubiläum. Debatte mit Stadträt_innen, Teil 1](https://forumaugsburg.de/s_2kommunal/Kommunalreform/201107_100-tage-klimacamp-diskussionsrunde-mit-den-stadtraet_innen/index.htm)

## 28.10.
- Augsburger Allgemeine: [Der Ton zwischen Radfahrern und der Stadt Augsburg verschärft sich](https://www.augsburger-allgemeine.de/augsburg/Der-Ton-zwischen-Radfahrern-und-der-Stadt-Augsburg-verschaerft-sich-id58425406.html)

## 23.10.
- On the air - Evangelische Kirche im Radio: kurz vor sechs
- Die Augsburger Zeitung: [Stadt im Schlagabtausch mit Fahrradaktivisten](https://www.daz-augsburg.de/stadt-im-schlagabtausch-mit-fahrradaktivisten-2)
- Augsburger Allgemeine: [Ist das Klimacamp rechtmäßig? Gerichtsurteil lässt auf sich warten](https://www.augsburger-allgemeine.de/augsburg/Ist-das-Klimacamp-rechtmaessig-Gerichtsurteil-laesst-auf-sich-warten-id58402501.html)
- Channel Welcome: [100 Tage Klimacamp Augsburg](https://www.channel-welcome.de/sendungen/beitraege/100-tage-klimacamp-augsburg/)

## 20.10.
- Radio Schwaben: [111 Tage Klimacamp – und was jetzt?](https://radioschwaben.de/nachrichten/augsburg-111-tage-klimacamp-und-was-jetzt/)

## 19.10.
- Lisa Badum: [Klimacamps in Augsburg und Nürnberg](https://www.lisa-badum.de/2020/10/19/klimacamps-in-augsburg-und-nuernberg/)

## 16.10.
- Bayerische Staatszeitung: [Gretas Jünger belagern das Augsburger Rathaus](https://www.bayerische-staatszeitung.de/staatszeitung/kommunales/detailansicht-kommunales/artikel/gretas-juenger-belagern-das-augsburger-rathaus.html#topPosition)

## 12.10.
- Zündfunk des BR: [100 Tage Klimacamp in Augsburg](https://www.br.de/radio/bayern2/sendungen/zuendfunk/zuendfunk-magazin-12102020_x-100.html) (ab Minute 2:55)

## 11.10.
- Augsburger Allgemeine: [Aktivisten des Klimacamps besetzen Baum in der Fuggerstraße](https://m.augsburger-allgemeine.de/augsburg/Aktivisten-des-Klimacamps-besetzen-Baum-in-der-Fuggerstrasse-id58311051.html)

## 10.10.
- Bayerischer Rundfunk: [100 Tage Klima-Camp - Keine Annäherung ans Augsburger Rathaus](https://www.br.de/nachrichten/bayern/100-tage-klima-camp-keine-annaeherung-ans-augsburger-rathaus,SCvXUaO)
- Rundschau des BR: [100 Tage Klimaprotestcamp in Augsburg](https://www.br.de/mediathek/video/rundschau-1830-10102020-corona-massnahmen-kassenaerzte-kritisieren-regelungswut-av:5f4f8abcbff613001b8303f8) (Minute 11:25)

## 9.10.
- Augsburger Allgemeine: [Aus Rücksicht auf Senioren: Klimaschützer besetzen anderen Baum](https://www.augsburger-allgemeine.de/augsburg/Aus-Ruecksicht-auf-Senioren-Klimaschuetzer-besetzen-anderen-Baum-id58302941.html)

## 8.10.
- Augsburger Allgemeine: [Klimacamp-Aktivisten wollen Baum am Kaufbach besetzen](https://www.augsburger-allgemeine.de/augsburg/Klimacamp-Aktivisten-wollen-Baum-am-Kaufbach-besetzen-id58296166.html)

## 7.10.
- Die Augsburger Zeitung: [Kommentar: Augsburgs Stadtregierung leidet an einer inneren Schwäche](https://www.daz-augsburg.de/kommentar-augsburgs-stadtregierung-leidet-an-einer-inneren-schwaeche/)

## 5.10.
- Stadtzeitung: [Am Klimacamp in Augsburg - Tag 96](https://www.stadtzeitung.de/region/augsburg/lokales/klimacamp-augsburg-tag-96-id213335.html)
- Augsburger Allgemeine: [Die Klimaaktivisten werfen der Stadt Augsburg Untätigkeit vor](https://augsburger-allgemeine.de/augsburg/Die-Klimaaktivisten-werfen-der-Stadt-Augsburg-Untaetigkeit-vor-id58264826.html)

## 25.9.
- Augsburger Allgemeine: [Fridays for Future streikt heute in Augsburg wieder fürs Klima](https://www.augsburger-allgemeine.de/augsburg/Fridays-for-Future-streikt-heute-in-Augsburg-wieder-fuers-Klima-id58198211.html)

## 23.9.
- Radio Schwaben: [Augsburg: Klimacamp muss kleiner werden](https://radioschwaben.de/nachrichten/augsburg-klimacamp-muss-kleiner-werden/)

## 23.9.
- Radio rt.1: [Im Oktober fällt die Entscheidung über das Augsburger Klimacamp](https://www.rt1.de/im-oktober-faellt-die-entscheidung-ueber-das-augsburger-klimacamp-188172/)

## 22.9.
- Augsburger Allgemeine: [Stadt Augsburg macht Auflagen: Klimacamper müssen sich einschränken](https://www.augsburger-allgemeine.de/augsburg/Stadt-Augsburg-macht-Auflagen-Klimacamper-muessen-sich-einschraenken-id58181041.html) (siehe auch [unsere inhaltliche Korrektur](/pages/Pressemitteilungen/2020-09-22-PM_Auflagen.html))

## 21.9.
- Augsburger Allgemeine: [Bürgerliche Fraktion treibt Schwarz-Grün beim Klimaschutz in Augsburg an](https://m.augsburger-allgemeine.de/augsburg/Buergerliche-Fraktion-treibt-Schwarz-Gruen-beim-Klimaschutz-in-Augsburg-an-id58162561.html)

## 20.9.
- Augsburger Allgemeine: [Klimawandel: Im Augsburger Rathaus ist mehr Tempo nötig](https://www.augsburger-allgemeine.de/augsburg/Klimawandel-Im-Augsburger-Rathaus-ist-mehr-Tempo-noetig-id58161006.html)
- Augsburger Allgemeine: [Der globale Klimawandel wird auch Augsburg hart treffen](https://www.augsburger-allgemeine.de/augsburg/Der-globale-Klimawandel-wird-auch-Augsburg-hart-treffen-id58144621.html)

## 19.9.
- Augsburger Allgemeine: [Die Deprimierten: Eine Nacht im Augsburger Klima-Camp](https://www.augsburger-allgemeine.de/augsburg/Die-Deprimierten-Eine-Nacht-im-Augsburger-Klima-Camp-id58123941.html)
- Forum solidarisches und friedliches Augsburg: [Anstehende Stadtratssitzung -- Bürgerliche Mitte und Augsburg in Bürgerhand wollen es wissen](https://forumaugsburg.de/s_2kommunal/Kommunalpolitik/200919_stadtratssitzung-24-september-antraege-zur-energiewende/index.htm)

## 18.9.
- Forum solidarisches und friedliches Augsburg: [80 Tage auf dem Fischmarkt -- Berichte aus dem Klimacamp](https://forumaugsburg.de/s_2kommunal/Kommunalreform/200918_berichte-vom-klimacamp/index.htm)

## 17.9.
- Stadtzeitung: [Hochbeet am Rathausplatz entfernt: Klimaaktivisten erstatten Anzeige gegen die Stadt](https://www.stadtzeitung.de/region/augsburg/lokales/hochbeet-rathausplatz-entfernt-klimaaktivisten-erstatten-anzeige-gegen-stadt-id212520.html)

## 16.9.
- Augsburger Allgemeine: [Ärger im Klimacamp: Streit zwischen AfD und Aktivisten geht weiter](https://www.augsburger-allgemeine.de/augsburg/Aerger-im-Klimacamp-Streit-zwischen-AfD-und-Aktivisten-geht-im-Internet-weiter-id58138936.html)
- Radio Schwaben: [Augsburg: Klimacamp sorgt erneut für Aufregung](https://radioschwaben.de/nachrichten/augsburg-klimacamp-sorgt-erneut-fuer-aufregung/)

## 15.9.
- a.tv: [77 Tage Klimacamp in Augsburg](https://www.augsburg.tv/mediathek/video/77-tage-klimacamp-in-augsburg/)
- Augsburger Allgemeine: [Polizei wirft AfD-Stadtrat aus dem Augsburger Klimacamp](https://www.augsburger-allgemeine.de/augsburg/Polizei-wirft-AfD-Stadtrat-aus-dem-Augsburger-Klimacamp-id58129586.html)
- Augsburger Allgemeine: [Ärger um AfD am Klimacamp: Diese Eskalation war unnötig](https://augsburger-allgemeine.de/augsburg/Aerger-um-AfD-am-Klimacamp-Diese-Eskalation-war-unnoetig-id58132096.html)

## 13.9.
- Augsburger Allgemeine: [Moria und Klimaschutz: In Augsburg gehen die Protestaktionen weiter](https://www.augsburger-allgemeine.de/augsburg/Moria-und-Klimaschutz-In-Augsburg-gehen-die-Protestaktionen-weiter-id58114026.html)

## 11.9.
- Augsburger Allgemeine: [Die Hermanstraße bekommt einen „Pop-up-Radweg" – aber nur für eine Stunde](https://www.augsburger-allgemeine.de/augsburg/Die-Hermanstrasse-bekommt-einen-Pop-up-Radweg-aber-nur-fuer-eine-Stunde-id58103426.html)
- Augsburger Allgemeine: [Die Stadt entfernt das Hochbeet der Klimacamp-Aktivisten an der Maxstraße](https://www.augsburger-allgemeine.de/augsburg/Die-Stadt-entfernt-das-Hochbeet-der-Klimacamp-Aktivisten-an-der-Maxstrasse-id58101506.html)
- hochbeet.stangl.eu:
    - [Hochbeet von Klimaaktivisten auf einem Parkplatz](https://hochbeet.stangl.eu/hochbeet-von-klimaaktivisten-auf-einem-parkplatz/)
    - [Hochbeetentfernung](https://hochbeet.stangl.eu/hochbeetentfernung/)

## 10.9.
- Bayerischer Rundfunk: ["Platz-Park": Augsburger Klimaaktivisten blockieren Parkplatz](https://www.br.de/nachrichten/bayern/platz-park-augsburger-klimaaktivisten-blockieren-parkplatz,SAAKXVZ)
- Augsburger Allgemeine: [Klimacamp-Aktivisten machen einen Parkplatz in der Maxstraße zum Beet](https://www.augsburger-allgemeine.de/augsburg/Klimacamp-Aktivisten-machen-einen-Parkplatz-in-der-Maxstrasse-zum-Beet-id58094576.html)

## 2.9.
- Augsburger Allgemeine: [Vier Monate Schwarz-Grün in Augsburg: In Teilen der CSU grummelt es](https://augsburger-allgemeine.de/augsburg/Vier-Monate-Schwarz-Gruen-in-Augsburg-In-Teilen-der-CSU-grummelt-es-id58031906.html)

## 28.8.
- DAV Augsburg: [Nachlese Augsburger Klimacamp & aktueller Klimaschutzbericht](https://www.dav-augsburg.de/aav/verein-berichte/1429-nachlese-augsburger-klimacamp-aktueller-klimaschutzbericht)

## 20.8.
- Augsburger Allgemeine: ["Pop-up-Radweg"-Protest: Hermanstraße könnte dauerhafte Radspur bekommen](https://outline.com/cG6c6E)

## 17.8.
- Augsburger Allgemeine: [Klimaschützer wollen den Winter über im Camp am Augsburger Rathaus bleiben](https://augsburger-allgemeine.de/augsburg/Klimaschuetzer-wollen-den-Winter-ueber-im-Camp-am-Augsburger-Rathaus-bleiben-id57939946.html)

## 15.8.
- Forum solidarisches und friedliches Augsburg: [Blockade von Premium Aerotec Werk III in Haunstetten – das etwas andere Friedensfest](https://www.forumaugsburg.de/s_2kommunal/Friedensstadt/200815_blockade-von-premium-aerotec-werk-iii-zum-friedensfest/index.htm)

## 12.8.
- Augsburger Hochschulmagazin presstige: [43 Tage Klimacamp – Ende offen](https://presstige.org/2020/08/43-tage-klimacamp-ende-offen/)
- Augsburger Allgemeine: [Verkehr, Klimaschutz und mehr: Wie nachhaltig ist Augsburg?](https://www.augsburger-allgemeine.de/augsburg/Verkehr-Klimaschutz-und-mehr-Wie-nachhaltig-ist-Augsburg-id57915371.html)

## 9.8.
- Süddeutsche Zeitung: [Friedenspreis für Kardinal Marx und Bedford-Strohm](https://www.sueddeutsche.de/politik/kommunen-augsburg-friedenspreis-fuer-kardinal-marx-und-bedford-strohm-dpa.urn-newsml-dpa-com-20090101-200808-99-91576)

## 8.8.
- Bayerischer Rundfunk: [Klimacamp protestiert zum Friedensfest vor Premium Aerotec](https://www.br.de/nachrichten/bayern/klimacamp-protestiert-zum-friedensfest-vor-premium-aerotec,S73t481)

## 7.8.
- Bayerischer Rundfunk: [Die Augsburger Hermanstraße wird zum Pop-up-Radweg](https://www.br.de/nachrichten/bayern/die-augsburger-hermanstrasse-wird-zum-pop-up-radweg,S6ypNyy)

## 6.8.
- MUCBOOK: [Demonstrationskultur in Zeiten von Corona: Was macht eigentlich Fridays for Future gerade?](https://www.mucbook.de/demonstrationskultur-in-zeiten-von-corona-was-macht-eigentlich-fridays-for-future-gerade/)

## 2.8.
- Stadtzeitung: [Augsburger Radbegehren hat nötige Unterschriften für Bürgerentscheid erreicht](https://www.stadtzeitung.de/region/augsburg/politik/augsburger-radbegehren-hat-noetige-unterschriften-fuer-buergerentscheid-erreicht-id210100.html) (Yeah!)

## 1.8.
- Die Augsburger Zeitung: [Radentscheid: 12.819 gezählte Unterschriften – Aktivisten fordern Taten von Stadtregierung](https://www.daz-augsburg.de/radentscheid-12-819-gezaehlte-unterschriften-aktivisten-fordern-taten-von-stadtregierung/)

## 31.7.
- Bayerischer Rundfunk: [Klima-Camp in Augsburg noch immer aktiv](https://www.br.de/nachrichten/bayern/klima-camp-in-augsburg-noch-immer-aktiv,S6KtNYf)
- Augsburger Allgemeine: [Umweltaktivisten ziehen vom Klimacamp in Augsburg aus auf die Straße](https://www.augsburger-allgemeine.de/augsburg/Umweltaktivisten-ziehen-vom-Klimacamp-in-Augsburg-aus-auf-die-Strasse-id57847431.html)

## 28.7.
- Die Augsburger Zeitung: [Zuspruch aus ganz Deutschland – auch Luisa Neubauer besucht das Augsburger Klimacamp](https://www.daz-augsburg.de/zuspruch-aus-ganz-deutschland-auch-luisa-neubauer-besucht-das-augsburger-klimacamp/)
- Bayerischer Rundfunk: [Klimacamp Augsburg: Luisa Neubauer kritisiert Stadtverwaltung](https://www.br.de/nachrichten/bayern/luisa-neubauer-klimacamp-augsburg-wahnsinnig-inspirierend,S629lzK)
- Radio rt.1: [Augsburger Klimacamp: Fridays-for-Future-Aktivistin Neubauer ruft Stadt zu mehr Klimaschutz auf](https://www.rt1.de/fridays-for-future-besuch-luisa-neubauer-von-augsburger-klimacamp-beeindruckt-180817/)

## 22.7.
- Stadtzeitung: [Was will das Augsburger Klimacamp erreichen? Die Forderungen der Umweltaktivisten](https://www.stadtzeitung.de/region/augsburg/politik/will-augsburger-klimacamp-erreichen-forderungen-umweltaktivisten-id209634.html)

## 19.7.
- Forum solidarisches und friedliches Augsburg: [Vortrag auf dem Klimacamp von Tobias Walter](https://www.forumaugsburg.de/s_2kommunal/Kommunalreform/200719_vortrag-auf-dem-klimacamp-dezentrale-energiewende-in-augsburg-jetzt/index.htm)
- Die Augsburger Zeitung: [Kommentar: Augsburgs Stadtregierung leidet an einer inneren Schwäche](https://www.daz-augsburg.de/kommentar-augsburgs-stadtregierung-leidet-an-einer-inneren-schwaeche/)

## 18.7.
- Radio Schwaben: [Augsburg: Klimacamp darf bleiben](https://radioschwaben.de/nachrichten/augsburg-klimacamp-darf-bleiben/)

## 17.7.
- Bayerischer Rundfunk: ["Klima-Camp" Augsburg: Aktivisten rechnen mit Eil-Entscheidung](https://www.br.de/nachrichten/bayern/klima-camp-augsburg-aktivisten-rechnen-mit-eil-entscheidung,S4yG3zh)
- Die Augsburger Zeitung: [Verwaltungsgericht Augsburg: Klimacamp ist vom Versammlungsrecht gedeckt](https://www.daz-augsburg.de/verwaltungsgericht-klimacamp-ist-vom-versammlungsrecht-gedeckt/)
- Aichacher Zeitung: [Klimacamp darf bleiben](https://www.aichacher-zeitung.de/vorort/augsburg/art21,157810)
- Bayerisches Verwaltungsgericht Augsburg: [Gericht gibt Eilantrag des „Klima-Camp“ statt](https://www.vgh.bayern.de/media/vgaugsburg/presse/klimacamp-versammlung.pdf)
- Hallo Augsburg: [Nachgefragt: Das passiert im Protestcamp am Rathaus](https://www.hallo-augsburg.de/klimacamp-augsburg-nachgefragt-das-passiert-im-protestcamp-am-rathaus_4W9)

## 15.7.
- Die Augsburger Zeitung: [Causa Klimacamp: Grüne in Not – Verwaltungsgericht soll entscheiden](https://www.daz-augsburg.de/causa-klimascamp-gruene-in/)

## 14.7.
- Stadtzeitung: ["Klimacamp" neben dem Rathaus wird vorerst noch nicht geräumt](https://www.stadtzeitung.de/region/augsburg/politik/klimacamp-neben-rathaus-vorerst-noch-geraeumt-id209199.html)
- Augsburger Allgemeine: [Das Klimacamp am Augsburger Rathaus wird vorerst nicht geräumt](https://www.augsburger-allgemeine.de/augsburg/Das-Klimacamp-am-Augsburger-Rathaus-wird-vorerst-nicht-geraeumt-id57725176.html)
- Radio Schwaben: [Augsburg: Klimacamp am Rathaus wird Fall fürs Gericht](https://radioschwaben.de/nachrichten/augsburg-klimacamp-am-rathaus-wird-fall-fuers-gericht/)

## 13.7.
- Süddeutsche Zeitung: [Wie "Fridays for Future" wieder in die Öffentlichkeit will](https://www.sueddeutsche.de/bayern/augsburg-fridays-for-future-camp-rathaus-1.4964426)
- Bayerischer Rundfunk: ["Klima-Camp" Augsburg: Aktivisten wollen vor Rathaus bleiben](https://www.br.de/nachrichten/bayern/klima-camp-augsburg-aktivisten-wollen-vor-rathaus-bleiben,S4ch3qw)

## 12.7.
* Oberbürgermeisterin Weber (CSU): [Klimacamp nicht von Versammlungsfreiheit gedeckt](https://www.facebook.com/evaweberaugsburg/photos/a.1065950733447370/4072563629452717/?type=3&source=57) (Wir sind entsetzt)
* Forum solidarisches und friedliches Augsburg: [Fridays for Future errichtet Camp neben dem Rathaus](https://www.forumaugsburg.de/s_6kultur/Unterricht/200712_fridays-for-future-errichtet-camp-neben-dem-rathaus-gegen-kohleausstiegsgesetz/index.htm)
* Bayerischer Rundfunk: [Klima-Demonstranten sollen Augsburger Rathausplatz verlassen](https://www.br.de/nachrichten/bayern/klima-demonstranten-muessen-augsburger-rathausplatz-verlassen,S4QK0pR)

## 11.7.
* Augsburger Allgemeine: [Politiker kritisieren angekündigte Räumung des Klimacamps](https://www.augsburger-allgemeine.de/augsburg/Politiker-kritisieren-angekuendigte-Raeumung-des-Klimacamps-id57713976.html)

## 10.7.
* Augsburger Allgemeine: [Die Stadt will das Klimacamp am Augsburger Rathaus räumen lassen](https://www.augsburger-allgemeine.de/augsburg/Die-Stadt-will-das-Klimacamp-am-Augsburger-Rathaus-raeumen-lassen-id57711651.html)
* Augsburger Allgemeine: [Augsburger Klimacamp: Die Stadt agiert kleinkariert](https://www.augsburger-allgemeine.de/augsburg/Augsburger-Klimacamp-Die-Stadt-agiert-kleinkariert-id57712471.html)

## 8.7.
- Stadtzeitung: [„Fridays for Future“ im Umweltausschuss ignoriert](https://www.stadtzeitung.de/region/augsburg/politik/fridays-future-umweltausschuss-ignoriert-id208926.html)

## 7.7.
- Augsburger Allgemeine: [Die Klimaschützer wollen ihr Camp am Rathaus noch lange betreiben](https://www.augsburger-allgemeine.de/augsburg/Die-Klimaschuetzer-wollen-ihr-Camp-am-Rathaus-noch-lange-betreiben-id57689296.html)

### 6.7.
- AUXPUNKS: [#RATHAUSbeCAMPen – RATHAUS BLOCKADE](https://auxpunks.uber.space/rathausbecampen-rathaus-blockade/)

## 3.7.
- Augsburger Allgemeine: ["Fridays for Future": Klimaschützer melden sich mit Protesten zurück](https://www.augsburger-allgemeine.de/augsburg/Fridays-for-Future-Klimaschuetzer-melden-sich-mit-Protesten-zurueck-id57671656.html)

## 2.7.
- Bayerischer Rundfunk: [Klimaaktivisten besetzen Augsburger Rathaus](https://www.br.de/nachrichten/bayern/klimaaktivisten-besetzen-augsburger-rathaus,S3eOSKj)

## 2.7.
- Augsburger Allgemeine: [Demo: Fridays for Future campen am Augsburger Rathausplatz](https://www.augsburger-allgemeine.de/augsburg/Demo-Fridays-for-Future-campen-am-Augsburger-Rathausplatz-id57665436.html)
- Augsburger Allgemeine: [Polizeiaufgebot am Rathausplatz: Aktivisten dringen in Rathaus ein](https://www.augsburger-allgemeine.de/augsburg/Polizeiaufgebot-am-Rathausplatz-Aktivisten-dringen-in-Rathaus-ein-id57666531.html)

## 1.7.
- Stadtzeitung: [Augsburger "Fridays for Future"-Bewegung ruft zu Rathaus-Blockade gegen das Kohleausstiegsgesetz auf](https://www.stadtzeitung.de/region/augsburg/politik/augsburger-fridays-future-bewegung-ruft-rathaus-blockade-gegen-kohleausstiegsgesetz-id208579.html)

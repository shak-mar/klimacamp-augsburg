---
layout: page
title: Adventskalender
permalink: /adventskalender/
nav_order: 10
---

# Der Klimaadventskalender des Klimacamps

Vom 1.12. bis zum 24.12. gibt es täglich neu veröffentlichte Videos vom
Augsburger Klimacamp. Die Inhalte sind noch am entstehen und reichen von
Erklärungen typischer Missverständnisse zum Thema Klimagerechtigkeit, über
Live-Musik, Gastbeiträge von Stadträt\*innen und Kirchenvertreter\*innen,
Erfahrungen und Beteiligungsmöglichkeiten im Stadtrat, bis hin zu Podcasts über
nun fast sechs Monate des Protestcamps. In einigen Fällen gibt es auch
begleitendes Material zum Weiterlesen und Recherchieren.

1. [Ankündigung](https://www.youtube.com/watch?v=_5fg9G-_jJA)
2. [Klimacamp Podcast #1](https://www.youtube.com/watch?v=BdUBZEoE2SI)
3. [Fakten zur Klimakrise #1](https://www.youtube.com/watch?v=XnVbOQVV_f8)
4. [Pressekonferenz Radentscheid](https://www.youtube.com/watch?v=xEYm--d-qqc)
5. [Klimacamp Podcast #2](https://www.youtube.com/watch?v=ihRqqGsAM-k)
6. [Adventsgrüße von Pfarrer\*innen Kleineidam der Dreifaltigkeitskirche](https://www.youtube.com/watch?v=aGkf3h3dxoM)
7. [Fakten zur Klimakrise #2](https://www.youtube.com/watch?v=O9ir_QUHbuE)
8. [Gastbeitrag von Peter Hummel (Freie Wähler, Fraktion Bürgerliche Mitte)](https://www.youtube.com/watch?v=6rdU-X7XYEE)
9. [Klimacamp Podcast #3](https://www.youtube.com/watch?v=4ukoNWaGnBs)
10. [Gastbeitrag von Anna Rasehorn (SPD) und Christine Wilholm (LINKE) zu Kreisverkehrantrag](https://www.youtube.com/watch?v=QeWunCP2DxA)
11. [Protest im Danni](https://www.youtube.com/watch?v=IPrcGrQOAsc)
12. [Fünf Jahre Pariser Klimaabkommen](https://www.youtube.com/watch?v=gWXFbuFgjzo)
13. [Adventsgrüße von Ordenspriester Jörg Alt](https://www.youtube.com/watch?v=Riffwsx4RZ8)
14. [Radentscheid Augsburg -- fünf Forderungen](https://www.youtube.com/watch?v=GM8W5Tv0YdA)
15. [Gastbeitrag von Lars Vollmar (FDP, Fraktion Bürgerliche Mitte) zu Solarzellen auf Parkflächen](https://www.youtube.com/watch?v=-2_hPUP543M)
16. [Klimacamp Podcast #4](https://www.youtube.com/watch?v=aL4-DvzyPAg)
17. [Gastbeitrag von Frederik Hintermayr (LINKE) zur Beschleunigung von Klimaanträgen](https://www.youtube.com/watch?v=DoWIJhkQ0js)
18. [Stadtratsprotokolle](https://www.youtube.com/watch?v=nTc_rEFIC_s)
19. [Klimacamp Podcast #5](https://www.youtube.com/watch?v=mfuyS7iRdK8)
20. [Adventsgrüße von Diakon Christian Wild von der Moritzkirche](https://www.youtube.com/watch?v=Xyv-iHOtthY)
21. [Pläne der Grünen in Augsburg](https://www.youtube.com/watch?v=nohE7suqSmE)
22. [Fantastische Live-Musik mit Mandàra](https://www.youtube.com/watch?v=zmW0rE73csQ)
23. [Keine Osttangente!](https://www.youtube.com/watch?v=WIA7azJsyyo)
24. [Türchen 24](https://www.youtube.com/watch?v=vbi4cFK1FTA) Frohe Weihnachtsfeiertage!

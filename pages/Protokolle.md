---
layout: page
title: Stadtratsprotokolle
permalink: /protokolle/
nav_order: 6
---

# Stadtratsprotokolle

Der Augsburger Stadtrat und seine Fachausschüsse verschließen der interessierten
Öffentlichkeit leider ausführliche Protokolle. Im
[Ratsinformationssystem der Stadt](https://ratsinfo.augsburg.de/bi/allris.net.asp)
sind nur die Abstimmungsergebnisse einsehbar, und sogar diese ohne Information
über das Abstimmverhalten der einzelnen Stadträt\*innen. Der argumentative
Austausch vor Abstimmungen fehlt völlig.

Gerade in Zeiten von Corona ist das besonders undemokratisch: Denn in der
Übertragung per Live-Stream vom Sitzungssaal in das Rathausfoyer sind nur die
Referent\*innen, nicht aber die Stadträt\*innen und daher auch nicht ihr
Abstimmungsverhalten zu sehen. Zudem ist die Video- und Tonqualität oft so
schlecht, dass ein aufmerksames Folgen in Details unmöglich ist. Der Live-Stream wird auch
nicht aufgezeichnet, obwohl es technisch leicht möglich wäre. Wer zur Zeit der
Sitzungen verhindert ist (das sind sicherlich die meisten Berufstätigen und
Sorgetätigen), kann sich also keinen Eindruck von der Stadtratsarbeit
verschaffen.

Wir nehmen daher die öffentliche Protokollierung selbst in die Hand. Die
Sitzungsprotokolle, die wir hier veröffentlichen, haben keinen Anspruch auf
Vollständigkeit und verstehen sich nicht als trockene Auflistung von
Wortbeiträgen. Stattdessen dokumentieren sie insbesondere den persönlichen
Eindruck, den die Stadträt\*innen unserem Protokollanten Alex Mai vermitteln,
sowie Brisantes und Interessantes.

Die Protokolle entstehen mit bestem Wissen und Gewissen. Trotzdem sind Fehler
möglich, insbesondere, da sie in Eile mitgeschrieben werden müssen. Wir
ermuntern den Stadtrat, die von der Verwaltung professionell
angefertigten Protokolle sowie Sitzungsaufzeichnungen (inklusive
Blick aufs Abstimmverhalten) selbst zu veröffentlichen.

* [25.06.2020: Stadtrat](/pages/material/Protokolle/2020-06-25-protokoll-stadtrat-mit-Bildern.pdf)
  -- Friedensbringer, 365-€-Ticket, Semmeltaste
* [23.07.2020: Stadtrat](/pages/material/Protokolle/2020-07-23-protokoll-stadtrat.pdf) --
  Lob von FFF (oh toll)
* [14.09.2020: Umweltausschuss](/pages/material/Protokolle/2020-09-14-umweltausschuss-prot-bisohneTOP4.pdf)
  -- MERCOSUR, Klimawandelanpassungskonzept, Baumfällungen
* [15.10.2020: Bauausschuss](/pages/material/Protokolle/2020-10-15-protokoll-bauausschuss-mit-Bildern.pdf)
  -- Fahrradabstellplätze, Radverkehrsführung
* [29.10.2020: Stadtrat/Hauptausschuss](/pages/material/Protokolle/2020-10-29-protokoll-stadtrat+hauptausschuss-mit-Bildern.pdf)
  -- Verschiebung der meisten TOPs in den Hauptausschuss, viel Corona
* [26.11.2020: Stadtrat](/pages/material/Protokolle/2020-11-26-protokoll-stadtrat-mit-Bildern.pdf)
  -- Straßenbahnlinie 5, Abfallgebührenerhöhung, Zoo, Atommüllendlager in
  Augsburg, erneutes Gerichtsverfahren gegen das Klimacamp
* [17.12.2020: Stadtrat](/pages/material/Protokolle/2020-12-17-protokoll-stadtrat-mit-Bildern.pdf)
  -- Linie 5, Klimaschutzsofortprogramm, Verschiebung von Weihnachten
* [25.01.2021: Umweltausschuss](/pages/material/Protokolle/2021-01-25-protokoll-umweltausschuss-mit-Bildern.pdf)
  -- CO₂-Budget, Energiestandards, Solarzellen

Übrigens: Die Opposition reichte seit Eröffnung des Klimacamps am 1. Juli
mehrere gute Anträge ein -- zum Radentscheid, für eine dezentrale Energiewende,
zum Klimanotstand, zur Analyse der Forderungen des Klimacamps, für eine
Stadtratssatzungsänderung, sodass klimarelevante Anträge zeitnaher besprochen
werden müssen, ... Diese wurden allesamt nicht zur Diskussion zugelassen.
Stattdessen machte eine unheilige Mischung aus Oberbürgermeisterin und
Verwaltung von ihrem dreimonatigen Vertagungsrecht Gebrauch.


# Andere Protokolle

* [10.10.2020: 100-tägiges Jubiläum des Klimacamps](/pages/material/Protokolle/2020-10-10-protokoll-gespraechsrunde-politik-jubilaeum-n.pdf)
  (stellenweise lückenhaft; ausführliche Aufarbeitung in zwei Artikeln des Forums solidarisches und friedliches Augsburg: [Teil 1](https://forumaugsburg.de/s_2kommunal/Kommunalreform/201107_100-tage-klimacamp-diskussionsrunde-mit-den-stadtraet_innen/index.htm), [Teil 2](https://www.forumaugsburg.de/s_2kommunal/Kommunalreform/201205_100-tage-klimacamp-diskussionsrunde-mit-den-stadtraet_innen-2/index.htm))

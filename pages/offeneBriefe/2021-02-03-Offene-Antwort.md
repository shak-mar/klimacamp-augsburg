---
layout: page
title:  "03.02.2021: Offene Antwort auf offenen Brief von Umweltreferent Reiner Erben"
date:   2021-02-03 23:30:00 +0200
categories: jekyll update
parent: "Offene Briefe"
nav_order: 3
---

# Offene Antwort auf offenen Brief von Umweltreferent Reiner Erben

Lieber Herr Erben,

vielen Dank für Ihren [offenen Brief](https://gruene-augsburg.de/home/news-detail/article/offener_brief_aus_dem_klimaschutzreferat_der_stadt_augsburg/).

<style>
  blockquote { border-left: 1em solid #ccc; padding-left: 1em; margin-left: 0; }
</style>

Wir möchten Sie zunächst um Entschuldigung bitten. Oberbürgermeisterin Weber
ließ uns taktisch in dem Glauben, dass es sie gewesen sei, die nach unserem
Gespräch mit ihr am 2.12.2020 das Klimaschutzsofortprogramm ausarbeitete.
Inzwischen wissen wir, dass das Klimaschutzsofortprogramm vom 17.12.2020 vor
allem Ihrer Feder und der Fachkompetenz anderer Grüner entstammt. Das möchten
wir hiermit ausdrücklich würdigen.

In der Sorge, dass das Programm sonst verworfen werden könnte, sicherten wir
Frau Webers persönlichem Referenten eine lobende Pressemitteilung zu, bei der
Sie aufgrund der von Frau Weber vorgetäuschten Urheberschaft Sie unerwähnt
blieben. Das tut uns leid. Wir werden zukünftig mit solch listigem Vorgehen nun
eher rechnen und Frau Weber stärker misstrauen müssen.

Wenn wir gewusst hätten, dass das Klimaschutzsofortprogramm Ihrer Feder
entstammte und so oder so, unabhängig von unserer Positionierung, in den
Stadtrat eingebracht worden wäre, hätten wir das Programm einerseits stärker
und damit ehrlicher kritisiert und andererseits den Vorstoß von Ihnen und den
Grünen deutlich honoriert.

Außerdem möchten wir anerkennen, dass Sie derzeit mit Corona keine leichte Zeit
haben. Der persönliche Angriff auf Sie im AZ-Kommentar dazu war unangemessen
und spiegelte nicht wieder, dass es strukturelle Gründe dafür gibt, dass in
Deutschland momentan kein\*e Gesundheitsamtchef\*in gut wegkommt.

> Liebe Aktive im Klimacamp,
>
> Liebe Aktive bei Fridays for Future Augsburg,
>
> über den Tenor der Pressemitteilung vom 16.01.2021 “Klimacamp besteht 200
> Tage” bin ich erstaunt. Ich möchte mich – auch im Namen der
> Oberbürgermeisterin – nicht rechtfertigen und aufzählen, wie oft
> unterschiedliche Vertreter\*innen der Stadt schon mit dem Klimacamp
> gesprochen haben.

Wir freuen uns, dass die Stadt nicht gerne als Klimazerstörer dastehen möchte.
**Unsere Messlatte ist die Umsetzung von konkreten und ernstzunehmenden
Klimagerechtigkeitsmaßnahmen.** Die wurden nicht in einem der Klimakrise
angemessenen Umfang ergriffen: Allein seit Beginn des Klimacamps am 1. Juli
2020 wurden 15 % des uns bei wohlwollender Rechnung noch übrigen CO₂-Budgets
verbraucht, das uns seit dem 1. Juli 2020 noch zur Verfügung steht. **Um das
Restbudget von 9,7 Millionen Tonnen CO₂ einzuhalten, müssen mittlerweile jedes
Jahr im Vergleich zum Vorjahr 19 % der Emissionen eingespart werden! Hätten wir
früher begonnen, wäre die nötige Reduktion deutlich niedriger. Je länger wir
jetzt warten, desto größer wird sie.**

Das erfordert gewaltige strukturelle Veränderungen wie den massiven Ausbau des
ÖPNV sowie seine radikale Vergünstigung, die Errichtung eines sicheren und
komfortablen durchgängigen Radwegenetzes; den sofortigen Ausstieg der
Stadtwerke aus der Kohleverstromung, die Freigabe von Flächen zum Windradbau,
die Installation von Solarzellen auf allen städtischen und privaten Gebäuden,
die nicht denkmalgeschützt sind, die Reaktivierung des Tarifs "Power@Home" der
Stadtwerke; den Schutz der vor Rodung bedrohten Wälder in Augsburg und der
unmittelbaren Umgebung; und viele andere mehr.

**Ein Plan für diese strukturellen Änderungen liegt aktuell nicht vor.**

Fast alle genannten Maßnahmen liegen im direkten Wirkungskreis der Stadt
Augsburg. Die Stadt darf nicht zurückscheuen, Haushaltsmittel dafür in die Hand
zu nehmen und, falls nötig, bei der Regierung von Schwaben die Aufnahme von
Schulden zu beantragen. Mit jedem Jahr, das wir ohne Beginn dieser
strukturellen Änderungen verstreichen lassen, wird die Klimakrise weiter
angeheizt und die Folgekosten für die Stadt Augsburg steigen immer weiter an.
Augsburg muss beginnen, Klimaschulden zurückzuzahlen statt wie bisher jedes
Jahr neue anzuhäufen!

Für die Maßnahmen, die nicht im Wirkungskreis der Stadt liegen oder Förderung
durch Bund und Land benötigen, muss sich die Stadt massiv und mit aller Kraft
für eine Änderung der Rahmenbedingungen einsetzen. **Dies muss nicht nur
innerhalb der zuständigen Gremien, sondern auch parteiintern und
öffentlichkeitswirksam erfolgen.** Auch diesen Prozess nehmen wir bisher nicht
wahr.

> Ich will auch nicht kritisch auf die genannten Beispiele aus anderen Kommunen
> eingehen, die sich bei genauerem Hinsehen vielleicht als gar nicht so
> wegweisend herausstellen, wie es zunächst scheinen mag.

Darauf hätten Sie ruhig eingehen können: Was die Klimagerechtigkeitsmaßnahmen
anderer Städte angeht, sind wir weniger informiert, und in der Eile der
Pressemitteilungsverfassung machten wir Fehler und waren wir nicht hinreichend
kritisch. Wir danken Ihnen für den Hinweis.

> In diesem Zusammenhang bin ich auch erstaunt darüber, dass nicht auch
> dargestellt wird, welche Grundlagen in Augsburg zur Verbesserungen des
> Klimaschutzes in den letzten Jahren geschaffen wurden.

Ist das die Aufgabe einer Pressemitteilung des Klimacamps? Wir begreifen uns
als Korrektiv, das die Öffentlichkeit über die unzureichenden Maßnahmen seitens
der Regierungen aufklärt. Bürger\*innen, die nicht die Zeit haben, sich tiefer
mit der Materie zu beschäftigen, haben das Gefühl, dass die Regierungen
Klimagerechtigkeit schon mehr oder weniger auf dem Schirm hätten und angemessen
verfolgen würden. Dies ist natürlich nicht der Fall, weder Deutschland noch
Augsburg befinden sich auf einem pariskompatiblen Reduktionspfad.

Mit "pariskompatibel" meinen wir dabei immer: geeignet, um mit mindestens einer
⅔-Wahrscheinlichkeit die Erdaufheizung auf 1,5 Grad zu beschränken.

> Ich stehe dazu und werde mich weiter dafür einsetzen,
>
> - dass die neuen Klimaschutzziele der Stadt Augsburg eingehalten werden
> (Festlegung des CO₂-Restbudgets auf 9,7 Mio. Tonnen, wie vom Klimabeirat
> empfohlen, wird dem Umweltausschuss am kommenden Montag als
> Beschlussempfehlung vorgelegt, siehe: Vorlage (augsburg.de)). 

Wir sind sehr glücklich, dass das Budget im Umweltausschuss bestätigt wurde.
Als wir unsere 200-Tages-Pressemitteilung verfassten, waren wir uns dessen
nicht sicher. Nun steht immer noch die Bestätigung im Stadtrat aus. Nachdem
Frau Weber sich in einem BR-Interview aber kürzlich dafür lobte, mit den 9,7
Millionen Tonnen sogar unsere Forderung von 11 Millionen Tonnen zu unterbieten,
sind wir diesbezüglich optimistisch. Dies ist aber eine falsche Aussage der
Oberbürgermeisterin. Frau Weber unterschlug dabei nämlich, dass beide Zahlen
von uns stammten und die größere Zahl noch auf das Jahr 2020 bezogen war.

**Im Übrigen möchten wir an dieser Stelle auch das Budget von 9,7 Millionen
Tonnen noch mal in aller Deutlichkeit kritisieren, obwohl die Zahl von uns
selbst als realistisches und mehrheitsfähiges Ziel genannt wurde.** Dieses zu
groß angesetzte Budget negiert unsere historische Verantwortung, indem es die
überdurchschnittlichen Emissionen Augsburgs seit Beginn der Industrialisierung
bewusst unterschlägt. Zudem bleiben Kippelemente wie der tauende
Permafrostboden in der sibirischen Tundra, der momentan noch Methan am
Entweichen hindert, unberücksichtigt. Zu guter Letzt basiert unsere Rechnung
auf dem IPCC-Bericht aus dem Jahr 2018. Dieser ist weichgewaschen, da er im
Konsensprinzip verabschiedet wurde und auch Länder wie Saudi-Arabien, deren
Wirtschaft vom Export fossiler Brennstoffe maßgeblich abhängt, ihre Zustimmung
ausdrücken mussten. Aktuelle europäische Studien geben viel kleinere
Restbudgetmengen an und auch der bald erscheinende nächste IPCC-Bericht wird
von einem sehr viel kleineren Budget ausgehen.

> Stadtpolitik und Stadtverwaltung werden die beschlossenen Einzelpunkte des
> Sofortprogramms so schnell wie möglich auf den Weg bringen und umsetzen.
> Damit dies mit der nötigen Gründlichkeit und demokratischen Legitimation
> geschehen kann - einzelne Maßnahmen müssen in den zuständigen Ausschüssen des
> Stadtrats diskutiert und beschlossen werden -, braucht es Zeit. Ich bitte
> darum, uns diese zuzugestehen.

Es sind nicht wir Klimagerechtigkeitsaktivist\*innen, die diesen Zeitdruck
herstellen, und "wir" sind dementsprechend auch nicht in der Position,
irgendwem Zeit zuzugestehen! Wir stützen uns auf die wissenschaftliche
Erkenntnis, dass die Beschränkung der Erdaufheizung auf 1,5 Grad notwendig ist,
um die Lebensgrundlage von uns Menschen sowie anderen Tieren und Pflanzen zu
sichern.

> Ich werde diese Prozesse so effizient wie möglich gestalten. 

Vielen Dank! Wir können den Druck, dem Sie und andere Politiker\*innen
ausgesetzt sind, nur erahnen. 
Dennoch unsere große Bitte an Sie: Bleiben Sie aktiv, werden Sie noch mutiger
und aktivieren Sie auch andere Politiker\*innen, die sich momentan noch nicht
aus der Deckung trauen. Die Zukunft Augsburgs und dadurch auch indirekt
Deutschlands und der Welt liegen in den Händen der Politiker\*innen. Seien Sie
mutig und helfen Sie, uns allen eine gute Zukunft zu bauen.

> Deshalb hoffe ich auch auf die Unterstützung und Solidarität des
> Klimacamps und der FFF Augsburg für den beschlossenen und noch zu beschließenden Klimapfad
> mit seinen   festgelegten und noch festzulegenden Einzelmaßnahmen. 

Für jedes Engagement, explizit auch Ihres, das uns in Richtung einer
klimagerechten Zukunft bringt, empfinden wir aufrichtige Wertschätzung. Dabei
ist solidarische Kritik aber nötig, wenn das Handeln nicht ausreicht, auch wenn
es ein Schritt nach vorne ist. Eine wichtige Rolle von uns als Bewegung sehen
wir darin, gesellschaftliche Mehrheiten für konkrete
Klimagerechtigkeitsmaßnahmen zu schaffen. Das allgemeine Ziel, Deutschland
sozial-ökologisch zu transformieren, genießt ja schon seit langem breite
Bevölkerungszustimmung. Deshalb ist es wichtig, in unseren jeweiligen
verschiedenen Rollen kritisch zusammenzuarbeiten.

Viele Grüße<br>
Ihre Aktivist\*innen vom Augsburger Klimacamp

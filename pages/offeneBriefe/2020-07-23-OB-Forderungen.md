---
layout: page
title:  "23.07.2020: Offener Brief der Aktivist*innen des Augsburger Klimacamps an Augsburgs Stadträt*innen"
date:   2020-07-23 07:00:00 +0200
categories: jekyll update
parent: "Offene Briefe"
nav_order: 1
---

# Offener Brief der Aktivist\*innen des Augsburger Klimacamps an Augsburgs Stadträt\*innen am 23. Juli 2020

[Offener Brief als PDF](/pages/material/offeneBriefe/2020-07-23_klimacamp_an_stadtrat.pdf).

Sehr geehrte Stadträt\*innen,

wir sind die Aktivist\*innen des Augsburger Klimacamps, die seit 23 Tagen
neben dem Rathaus protestieren. Unserem Camp gingen eineinhalb Jahre
großer Demonstrationszüge voraus, die zwar den gesellschaftlichen
Diskurs substanziell verschoben, aber keinerlei Auswirkungen auf
konkrete Beschlüsse im Stadtrat, Landtag oder Bundestag hatten.

1. Die Kohleverlängerung bis 2038 ist nicht mit dem 1,5-°C-Ziel des
Pariser Klimaabkommens vereinbar. Dies ist keine Behauptung von uns
Aktivist\*innen, sondern Resultat wissenschaftlicher Studien [1]. Deshalb
fordern wir die Bundesregierung auf, alle nötigen Schritte einzuleiten,
um das Gesetz zurückzunehmen und durch ein wirksames Gesetz zu ersetzen.
Die Stadt Augsburg hat somit die Verantwortung, sich entschieden und
öffentlichkeitswirksam gegen das Kohlegesetz zu positionieren.

2. Die Stadt Augsburg soll transparent und öffentlich ihr CO₂-Budget
kommunizieren: Wie viele Millionen Tonnen gedenkt Augsburg noch zu
emittieren? Falls das Budget die vom Weltklimarat IPCC veröffentlichte und
proportional auf Augsburg umgerechnete Menge übersteigt, soll eine schlüssige
Begründung angegeben werden, mit welchem Recht sich Augsburg diese
Überschreitung herausnimmt.

3. Bis Ende 2021 soll ein ausführlicher Plan zur Einhaltung des aufgestellten
CO₂-Budgets erstellt werden. Dieser Plan muss für die Öffentlichkeit
nachprüfbare Zwischenziele enthalten und in allen Aspekten sozial gerecht
sein. Personengruppen, die besonders vom Klimawandel betroffen sind, wie
etwa ärmere Menschen und Frauen, dürfen nicht unter den ergriffenen
Maßnahmen zusätzlich leiden.

Als konkrete Maßnahme, um das Augsburger CO₂-Restbudget einzuhalten,
fordern wir einen sofortigen Start einer dezentralen Energiewende hier
vor Ort in Augsburg, mit den Stadtwerken als verlässlichen
Dienstleister. Eine solche Energiewende intensiviert die Wertschöpfung
in der Region maßgeblich, stärkt den regionalen Handel und stellt
Versorgungssicherheit her. Damit können wir in Augsburg zügig bis 2025
aus der Kohle aussteigen. Ebenfalls lokal umsetzbar und mit geringen
Kosten verbunden sind die Ziele des Bürger\*innenbegehrens „Fahrradstadt
jetzt“.

Wir bitten Sie: Seien Sie mutig, profilieren Sie sich als
Schlüsselpersonen für Klimagerechtigkeit in Augsburg und handeln Sie.
Sie als gewählte Stadträt\*innen können die entscheidenden Hebel in
Bewegung setzen. Bitte nehmen Sie sich drei Minuten Zeit, um den
Kurzbericht [2] des WDR anzusehen. Er erläutert die Folgen des
Klimawandels auf fundierte und eingängige Art und Weise.

Erinnerung: Wir campen, bis ihr handelt.

Ihre Aktivist\*innen vom Augsburger Klimacamp  
Am Klimacamp 1  
86150 Augsburg

[1] [https://www.scientists4future.org/defizite-kohleausstiegsgesetz-kvbg-e/](https://www.scientists4future.org/defizite-kohleausstiegsgesetz-kvbg-e/)  
[2] [https://www.youtube.com/watch?v=FoMzyF_B7Bg](https://www.youtube.com/watch?v=FoMzyF_B7Bg)

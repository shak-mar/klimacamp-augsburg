---
layout: page
title:  "02.10.2020: Aktivist*innen des Augsburger Klimacamps überreichen umfangreichen Fragenkatalog an die Stadtwerke Augsburg"
date:   2020-10-02 07:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 20
---

*Pressemitteilung vom Augsburger Klimacamp am 2. Oktober 2020*

# Aktivist\*innen des Augsburger Klimacamps überreichen umfangreichen Fragenkatalog an die Stadtwerke Augsburg

**[Mehr Infos zum offenen Brief](/pages/offeneBriefe/2020-10-02-swa-Fragen.html)**  
**[Zum Fragenkatalog](/pages/material/Fragenkatalog-Stadtwerke/fragenkatalog.pdf)**

Genau eine Woche nach den Demonstrationen in mehr als 2500 Städten weltweit für
Klimagerechtigkeit am 25.9.2020 überreichen Aktivist\*innen des Augsburger
Klimacamps einen ausführlichen Fragenkatalog an die Stadtwerke Augsburg. Ziel
soll sein, eine Diskussionsgrundlage für die Rolle der Stadtwerke
bei der Energiewende und Abmilderung der Klimakrise zu sammeln und die
Stadtwerke an ihre herausragende Verantwortung zu erinnern.

„Die Stadtwerke haben in Augsburg eine besondere Rolle. Wir
Augsburger\*innen sind fast zwangsweise ihre Kund\*innen. Mit Strom, Heizen und
Verkehr sind die Stadtwerke in drei Bereichen aktiv, die für Klimagerechtigkeit
von entscheidender Bedeutung sind.“, erklärt Klimacamperin Sarah Bauer (16)
ihre Motivation, sich mit den Stadtwerken zu beschäftigen. „Aus diesem Grund
hatten wir auch schon den Chef der Stadtwerke sowie den Leiter ihrer
Energiesparte für ein Gespräch bei uns im Camp.“ Und da die Stadtwerke ihren
eigenen klimafreundlichen Tarif 'Power@Home' völlig unzureichend bewerben,
sahen die Aktivist\*innen des Klimacamps sich gezwungen, selbst seine Bewerbung
in die Hand zu nehmen [1].

Die Rolle der Stadtwerke sieht das Klimacamp mit gemischten Gefühlen.
„Der öffentliche Nahverkehr befreit zumindest diejenigen von uns, die es sich
leisten können und deren Wege vom ÖPNV erschlossen sind, vom Zwang, ein eigenes
Auto zu besitzen. Ladesäulen der Stadtwerke ermöglichen es, falls notwendig,
ein Elektroauto zu besitzen ohne eigene Ladeinfrastruktur bei sich zu Hause zu
haben. Mit Wasserkraft, Windkraft und Solarkraft produzierter Strom hat bereits
einen beachtlichen Anteil am Gesamtstrommix der swa.“, erläutert Bauer.

„Allerdings gibt es auch den Eindruck, dass die Ambitionen hinter dem Nötigen
und leider auch hinter dem Möglichen zurück bleiben.“, kritisiert Bauers
Mitcamper und Mathematikstudent Alexander Mai (24). Beispielsweise ist ein
Anteil von 21,8% Kohlestrom am Strommix von 2018 für bayrische Verhältnisse
hoch. „Das an den Tag gelegte Tempo war vor 20 oder 30 Jahren, als wir noch
mehr Zeit hatten, angebracht, aber für 2020 zu langsam.“

Die Stadtwerke Augsburg befinden sich komplett im Eigentum der Stadt. Hier
zeigen sich die Auswirkungen der Stadtpolitik, an die sich wesentliche Teile
der Forderungen der Klimacamper\*innen richten. „Daher wollen wir uns nun ein
genaueres Bild der Stadtwerke Augsburg machen.“

Das Format als offener Brief erlaubt es den Stadtwerken, mehr Zeit für
Recherche in die Beantwortung von Fragen zu stecken als dies in in einem
Gespräch der Fall ist. Mehrere Personentage an Arbeit und Zusammenarbeit mit
Wissenschaftler\*innen sind in die Erstellung des Fragenkatalogs eingeflossen.
Die Schüler\*innen im Camp hoffen, dass sich die Stadtwerke ähnliche Mühe mit
der Beantwortung geben werden und gemeinsam Klimagerechtigkeit in Augsburg
vorangebracht wird.

[1] https://augsburg.klimacamp.eu/pages/Pressemitteilungen/2020-08-22-PM_Stadtwerke.html

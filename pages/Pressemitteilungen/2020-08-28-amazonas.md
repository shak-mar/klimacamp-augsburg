---
layout: page
title:  "28.08.2020: Weltweite Aktionen für den Erhalt des Amazonas und gegen das EU-Freihandelsabkommen Mercosur"
date:   2020-08-28 07:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 13
---

*Pressemitteilung vom Augsburger Klimacamp am 28. August 2020*

# Weltweite Aktionen, auch in Augsburg, für den Erhalt des Amazonas und gegen das EU-Freihandelsabkommen Mercosur

Der Amazonas Regenwald ist eine der artenreichsten Regionen der Erde,
CO₂-Speicher und Sauerstoffquelle und trägt zur Stabilisierung des
Weltklimas bei. Die Zerstörung des Amazonas ist uns allen ein trauriger
Begriff. In den letzten Monaten nahmen dennoch Brände und Abholzungen
dramatisch zu. Bereits im August dieses Jahres wurden über 10.000
Feuerhotspots im Amazonas registriert. Von Januar bis März gingen 800
km² Wald verloren, das entspricht einer Fläche von 100.000
Fußballfeldern bzw. etwa einem Drittel der Größe des Saarlandes. 51 %
mehr als im Vorjahr! Das erschreckende: Ist einmal ein zu großer Teil
des Waldes zerstört, ist er zum Sterben verurteilt. Das muss verhindert
werden, da es sich bei diesen um einen klimatischen Kipppunkt handelt.

Die brasilianische Regierung bestätigte in einem Skandal, dass ihnen der
Regenwald und damit die jetzigen und folgenden Generationen wenig
kümmert. Vielmehr werden finanzielle Interessen verfolgt.
„Wir haben jetzt die Möglichkeit, da die Presse sich ausschließlich mit
Covid-19 beschäftigt, uns das Amazonas-Thema vorzunehmen. Wir haben in
diesem Moment die Chance, alle Regelungen zu ändern und die Vorschriften
zu vereinfachen“, sagte der brasilianische Umweltminister am 22. April [1].

Am heutigen Freitagnachmittag platzierten Augsburger Aktivist\*innen des
Klimacamps Banner zwischen zwei Bäumen am Königsplatz (Foto zur freien
Verwendung angehängt) Währenddessen sprachen sie mit Passant\*innen und
klärten diese über die katastrophale Lage auf. Es war nicht das erste
Mal, dass Aktivist\*innen in Augsburg auf die Zerstörung des Regenwalds
aufmerksam machten. Fridays for Future Augsburg veranstaltete vor der
Corona-Pandemie bereits Demonstrationen zu dem Thema mit mehreren
tausend Teilnehmer\*innen und Greenpeace arbeitet seit Jahren
kontinuierlich zum Schutz des Amazonas. In einer Diskussion im
Augsburger Klimacamp wurde jedoch klar, dass sich die Situation seitdem,
um ein Vielfaches verschlimmert hat: Augsburgs Bürger\*innen sollten
darüber informiert und zum Handeln angeregt werden, dezentrale Aktionen
sind auch mit 9.255,50 km Entfernung ein Muss.

„Umweltschutz- und Klimagerechtigkeitsbewegungen müssen in Brasilien mit
erheblicher Repression durch die korrupten Regierung rechnen. Deshalb
ist es umso wichtiger, dass wir in Deutschland und Europa unsere
Verantwortung ernstnehmen und Druck gegen diesen Ökozid ausüben“, sagte
Leon Ueberall (17) in einer Diskussionsrunde im Klimacamp.

Vom 28.8. bis zum 30.8. finden weltweit Aktionstage für den Erhalt des
Amazonas und gegen das EU-Freihandelsabkommen Mercosur statt. Das
Handelsabkommen sieht vor, Zölle auf Agrarprodukte wie Rind- und
Geflügelfleisch, Zucker, Bioethanol und Futtersoja zu senken. Für diese
Produkte werden Regenwälder gerodet und abgeholzt. Zusätzlich dazu
werden deutsche Kleinbauern immer mehr dem Konkurrenzdruck ausgeliefert.
In Augsburg werden weitere Aktionen folgen.

Referenzen:  
[1] [https://www.fr.de/wirtschaft/brasilien-corona-krise-regenwald-abholzung-13776889.html](https://www.fr.de/wirtschaft/brasilien-corona-krise-regenwald-abholzung-13776889.html)

Mehr Informationen:  
[https://act.greenpeace.de/eumercosur](https://act.greenpeace.de/eumercosur)  
[https://fridaysforfuture.de/rettet-den-amazonas-jetzt-weltweite-aktionstage-vom-28-30-08-2020/](https://fridaysforfuture.de/rettet-den-amazonas-jetzt-weltweite-aktionstage-vom-28-30-08-2020/)

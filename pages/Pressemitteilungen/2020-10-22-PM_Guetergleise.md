---
layout: page
title:  "22.10.2020: Aktivist*innen protestieren mit einer Kunstaktion gegen laufenden Gleisrückbau am Augsburger Hauptbahnhof"
date:   2020-10-22 23:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 24
---

*Pressemitteilung vom Augsburger Klimacamp am 22. Oktober 2020*

# Aktivist\*innen protestieren mit einer Kunstaktion gegen laufenden Gleisrückbau am Augsburger Hauptbahnhof

Diesen Samstag (24.10.2020) führen die Aktivist\*innen des Augsburger
Klimacamps von 14:30 Uhr bis 17:30 Uhr am Rathausplatz eine Kunstaktion
vor. Aus Holz werden Schienen und eine Lore gebaut, um gegen den weithin
unbemerkten aber momentan laufenden Gleisrückbau am Augsburger
Hauptbahnhof zu demonstrieren.

Vor einigen Wochen stellten das Bundesverkehrsministerium und der
DB-Konzern den "Masterplan Schiene" feierlich der Öffentlichkeit vor.
Durch den Masterplan Schiene soll die Schieneninfrastruktur ausgebaut
werden, um im Verkehrssektor Klimaziele zu erreichen. 

Doch abseits der Öffentlichkeit wird in Augsburg das Gegenteil
umgesetzt: Die DB Netz AG baut gerade ein 700 Meter langes Gleis ab.
"Das Gleis wurde bisher zum Bilden und Auflösen von Güterzügen
benötigt", erklärt Klimacamperin und Bahninteressierte Romina Dreyer (23).
"Leidtragende sind verschiedene Güterbahnen wie DB Cargo, Augsburger
Localbahn und Staudenbahn sowie die Industriebetriebe. Eine ausreichend
große Menge an Gleisen ist wichtig für die Kapazität des Güterbahnhofes
Augsburg. Andernfalls müssen Güter per LKW transportiert werden, was
nicht mit der Mobilitätswende und letztendlich der 1,5-Grad-Marke des
Pariser Klimaabkommens kompatibel ist."

Waren vor dem Umbau die alten Fundamente in der Stützmauer verankert,
stehen die neuen Fundamente und Fahrleitungsmasten nun direkt im
Gleisbett. Ein Ersatz für das 700 Meter lange Güterzuggleis ist nicht in
Aussicht. "Als Grund wird die enge städtebauliche Bebauung genannt.
Zwischen Augsburg Hbf und Oberhausen befindet sich südlich der
Wertachbrücke eine Schrebergarten Siedlung", so Dreyer.

Bereits im Sommer 2019 machte Fridays for Future mit einem Apell an
Kommunalpolitik und Landespolitik auf die voranstehende Gleisstillegung
aufmerksam und forderte, die neuen Masten außerhalb des Gleisbettes zu
setzen. "Wir informierten deshalb das Bundesverkehrsministerium",
erklärt Samuel Bosch (17), Aktivist des Klimacamps und
Versammlungsleiter der Kunstaktion. "Das Bundesverkehrsministerium
zeigte in seiner Antwort jedoch kein echtes Sachverständnis und übernahm
unreflektiert die Argumentation der DB Netz AG."

Wie in Augsburg Hbf. der zunehmende Güterverkehr mit weniger Gleisen
abgewickelt werden soll, ist unklar. "Im Raum Augsburg wird nach
Inbetriebnahme des neuen Container-Terminals mit einer Steigerung der
Zugzahlen gerechnet", erklärt Dreyer. "Die Mobilitätswende verlangt
nicht den Rückbau, sondern den Ausbau des Schienennetzes!", mahnt daher
Bosch.

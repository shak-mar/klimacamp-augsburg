---
layout: page
title:  "01.08.2020: Tränen und Freude bei Politiker*innengespräch im Klimacamp"
date:   2020-07-28 07:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 5
---

*Pressemitteilung vom Augsburger Klimacamp am 1. August 2020*

# Tränen und Freude bei Politiker*innengespräch im Klimacamp

Foto ab 16:40 Uhr herunterladbar unter folgender Adresse:
https://www.speicherleck.de/iblech/stuff/foto-2020-08-01 (zur freien
Verwendung)

Seit dem 1. Juli campen Aktivist\*innen von diversen Initiativen neben dem
Augsburger Rathaus, um sich für Klimagerechtigkeit einzusetzen. Anlässlich des
traurigen einmonatigen Bestehens luden sie heute Politiker\*innen ins Camp ein.
Der Einladung folgten die Bundestagsabgeordnete Ulrike Bahr (SPD) sowie die
Stadträt\*innen Peter Hummel (FW), Christian Pettinger (ÖDP), Lars Vollmar (FDP)
und Christine Wilholm (Linke).

Zu Tränen bei mehreren Aktivist\*innen führte ein Wortbeitrag von Ulrike Bahr.
Sie wurde gefragt, wieso sie im Bundestag für das Kohleeinstiegsgesetz stimmte.
Dieses Gesetz war der ursprüngliche Anlass für die Errichtung des Klimacamps;
es macht nach wissenschaftlichen Studien [1] die Einhaltung des Pariser
Klimaabkommens durch Deutschland unmöglich und bedroht so unmittelbar die
Zukunft der Jugendlichen und kommenden Generationen. Bahr ignorierte, dass sich
die Klimagerechtigkeitsbewegung mit den Kohlearbeiter\*innen durchaus und mit
Nachdruck solidarisiert, und verwies trotzdem auf diese Arbeiter\*innen, auf
Fraktionszwang und darauf, dass ein schlechtes Gesetz besser als gar keins sei.
Die Aktivist\*innen hatten nicht den Eindruck, dass Bahr die Schwere und
Dringlichkeit der Klimakrise bewusst war.

Für Begeisterung sorgte dagegen ein Vorschlag von Christian Pettinger, der von den
anderen Stadträt\*innen sofort aufgegriffen wurde. Immer wieder werden im
Stadtrat dringliche Entscheidungen, die die Bekämpfung der Klimakrise
betreffen, auf spätere Sitzungen verschoben. Zuletzt war dies in der letzten
Stadtratssitzung vor der Sommerpause der Fall, als die Diskussion zum
sofortigen Beginn der dringend nötigen Energiewende in Augsburg um drei Monate
vertagt wurde (Antrag von Augsburg in Bürgerhand). Die anwesenden
Stadträt\*innen streben nur eine Satzungsänderung an, sodass klimarelevante
Anträge mit absoluter Dringlichkeit bearbeitet werden müssen.

„Diese Satzungsänderung sollte ja auch im Interesse der Stadtregierung sein,
schließlich behauptet Oberbürgermeisterin Weber, Augsburg zur
klimafreundlichsten Stadt Bayerns machen zu wollen“, kommentierte Janika
Pondorf (16) diesen Vorschlag.

Paula Stoffels (17) kündigte zum Ende der zweistündigen Gesprächsrunde an, dass
weitere Einladungen folgen werden. „Wir wissen es sehr zu schätzen, dass Sie
sich heute so viel Zeit nahmen“, freute sich Stoffels. Dem schlossen sich die
aktivistische Gäste aus 12 weiteren Städten, darunter Bern, Berlin und Hamburg, an.
„Die Klimakrise macht keine Ferien, und wir auch nicht. Weitere Aktionen werden
folgen, um immer wieder Klimagerechtigkeit auf die Tagesordnung zu setzen.“


## Hintergrundinformation:

Während vor einigen Wochen die Betreiberfirmen in Spanien die Hälfte ihrer
Kohlekraftwerke aufgrund von mangelnder Rentabilität abschalteten, werden durch
das Kohleeinstiegsgesetz den deutschen Betreibern bis 2038 Steuergelder gezahlt, um
künstlich die Kohle am Leben zu halten. Dieses Geld könnte sinnvoller
eingesetzt werden -- nicht zuletzt zur Versorgung der Kohlearbeiter\*innen.
Das Kohleeinstiegsgesetz bleibt sogar noch hinter den Empfehlungen der
Kohlekomission zurück.

---
layout: page
title:  "15.12.2020: Ein Hoffnungsschimmer am Horizont -- von Oberbürgermeisterin Weber persönlich auf den Weg gebrachte Stadtratsentscheidung"
date:   2020-12-15 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 31
---

*Pressemitteilung vom Augsburger Klimacamp am 15. Dezember 2020*

# Ein Hoffnungsschimmer am Horizont -- von Oberbürgermeisterin Weber persönlich auf den Weg gebrachte Stadtratsentscheidung

Gestern stellte Oberbürgermeisterin Weber ein von ihr persönlich auf den Weg
gebrachtes Klimaschutzsofortprogramm vor [1]. Die Aktivist\*innen des Augsburger
Klimacamps begrüßen das Programm als einen Schritt hin zu einer klimagerechten
Stadt, der eine Ausgangsbasis für weitere Entwicklungen darstellen und
perspektivisch den Weg für einen Abbau des Camps ebnen kann. Dem Programm
mangelt es aber an der Verabschiedung einer der Klimakrise angemessenen
Zielsetzung und an dringend erforderlichen mutigen Maßnahmen, die alle
hinlänglich bekannt sind.

In Punkt 1 der Beschlussvorlage bekennt sich zwar die Stadt erneut zu ihrer
Verantwortung zum Pariser Klimaabkommen. "Das Programm gibt aber kein konkretes
Restbudget an Treibhausgasemissionen an, das dieser Verantwortung gerecht
werden würde", kommentiert Klimacamperin Lara Sylla (16). Die aktuellen
Klimaschutzziele Augsburgs sehen bereits 2025 eine Überschreitung des Budgets
vor [2]. Viele der Punkte sind lediglich Prüfaufträge oder wenig konkret.
"Zudem bringen gute Vorsätze allein leider nicht genug, wenn sie nicht mit
mutigen und notwendigen Maßnahmen gestützt werden, wie sich schon beim Projekt
der Fahrradstadt 2020 zeigte. Das Sofortprogramm stellt weder
Verwaltungsstellen noch Haushaltsmittel in Aussicht, die zur Umsetzung benötigt
werden."

Die Pandemie erschwert die finanzielle Lage Augsburgs. "Dies ist jedoch der
perfekte Zeitpunkt. Es werden große Konjunkturpakete geschnürt werden. Diese
bieten die einzigartige Chance, die Wirtschaft und Gesellschaft in
kürzestmöglicher Zeit klimafreundlich umzubauen", ergänzt Syllas Mitstreiter
Fabian Theenhaus (17). "Die Stadt muss über alle ihr verfügbaren Kanäle Druck
auf das Land und den Bund ausüben, um diese zu geeigneten Weichenstellungen und
Zuschüssen zu bewegen. Dazu gehören nicht nur der Bayerische und der Deutsche
Städtetag, sondern auch der Weg über innerparteiliche Vernetzung und
medienwirksame öffentliche Stellungnahmen". In diese Richtung enthält der
Beschlussvorschlag nichts.

Die Klimacamper\*innen sind erfreut über Punkt 10 der Beschlussvorlage, den sie
als Vorbereitung der Gründung eines repräsentativen Bürger\*innenrats zur
Klimapolitik deuten. "Von den täglichen Gesprächen mit Passant\*innen sind wir
überzeugt, dass Augsburgs Bevölkerung kluge Entscheidungen in Sachen
Klimagerechtigkeit treffen könnte", erklärt Theenhaus. Etwa empfahl in
Frankreich ein solcher Rat -- zufällig aus der Bevölkerung zusammengesetzt, von
Expert\*innen beraten und keinem Wiederwahldruck ausgesetzt -- vermeintlich
radikale Maßnahmen wie ein Tempolimit von 110 auf Autobahnen und ein Verbot von
Inlandsflügen [3]. "Durch den repräsentativen Bevölkerungsquerschnitt wird in
solchen Bürger\*innenräten sichergestellt, dass Klimaziele auf sozial gerechte
Art und Weise umgesetzt werden und nicht etwa Maßnahmen getroffen werden, die
ärmere Haushalte besonders belasten." Augsburgs Klimagerechtigkeitsbewegung,
insbesondere die Initiative Extinction Rebellion, hatte einen solchen Rat schon
lange gefordert.

"Auf der einen Seite ist die Beschlussvorlage ein großer Erfolg für Augsburg
und die Klimagerechtigkeitsbewegung", resümiert Klimacamperin Janika Pondorf
(16). "Auf der anderen Seite reicht sie leider noch lange nicht aus, dass wir
sagen könnten: 'Um unsere klimatische Existenzgrundlage und die unserer Kinder
müssen wir uns keine Sorgen mehr machen.' Darin liegt ja die ganze Tragik der
Geschichte. Wir erwarten daher, dass die Stadtregierung der Klimakrise mit
derselben Ernsthaftigkeit begegnet, mit der sie der Pandemie gegenübertritt."

[1] https://ratsinfo.augsburg.de/bi/vo020.asp?VOLFDNR=10900
[2] https://augsburg.klimacamp.eu/co2-budget/
[3] https://www.zeit.de/politik/ausland/2020-06/klimapolitik-frankreich-buergerrat-klimaschutz-gelbwesten-direkte-demokratie
[4] https://www.erlangen.de/desktopdefault.aspx/tabid-1745/4659_read-38080/


## Einzelanalyse

Punkt 1: Gut; wenig konkret, keine explizite Nennung eines Budgets.

Punkt 2: Die Verabschiedung eines angemessenen Augsburger Energiestandards wäre
ein großer Schritt. Eine Beurteilung von Punkt 2b hängt aber vom Inhalt des in
Aussicht gestellten Standards ab. Punkt 2a ist nicht neu, sondern in der
Gründung des Klimabeirats enthalten. Punkt 2c ist ein reiner Prüfungsantrag.
Punkt 2d ist gut.

Punkt 3: Die aktuellen Studienziele sind ungenügend, Augsburg würde bei ihrer
Einhaltung das Augsburg zustehende CO2-Restbudget um das dreifache übersteigen.
Eine Katastrophe! Sehr gut sind aber die in Aussicht gestellten sektoriellen
Jahreszwischenziele, Steuerungsinstrumente zur Nachjustierung und öffentliche
jährliche Bilanzierung. Das kürzlich im Landtag beschlossene "Bayerische
Klimaschutzgesetz" wurde etwa vielfach von wissenschaftlicher Seite kritisiert,
diese Aspekte nicht zu enthalten und daher über den Status einer vagen
Absichtserklärung nicht hinauszukommen. In dieser Hinsicht setzt die
Beschlussvorlage der Verantwortungslosigkeit des Landtagsgesetzes also viel
entgegen. Auch gut ist, dass auf soziale Gerechtigkeit und die langfristige
statt kurzfristige Wirtschaft geachtet werden soll. All die aufgezählten
positiven Besonderheiten von Punkt 3 wurden auf unseren Vorschlag in die
Beschlussvorlage übernommen.

Punkte 4 und 5: Haben Potenzial, hängen stark von ihrer Umsetzung ab, könnten
sinnvolles Werkzeug oder reine PR-Nummern sein.

Punkt 6: Die Verhandlungen laufen bereits.

Punkt 7: Mit jedem Tag, den die Stadtwerke Augsburg den im Graustrom
enthaltenen Kohlestrom einkaufen, beteiligt sich Augsburg an der Top-1-Quelle
an Klimazerstörung, den Kohlekraftwerken, und an der Zerstörung von Dörfern in
Kohlerevieren, die neuen Kohlegruben weichen müssen. Die Stadtwerke müssen
daher so schnell wie möglich aus der Kohleverstromung aussteigen. In Erlangen
wurde am 26.11.2020 in einer historischen Stadtratssitzung beschlossen [4], dass
die dortigen Stadtwerke ab Ende 2022 nur noch Ökostrom einkaufen sollen. Weil
man vor Vertragsbruch zurückschreckt, ist eine noch schnellere Umstellung auch
nicht möglich. Augsburg müsste so schnell wie möglich folgen. Auf Seite 8 der
Beschlussvorlage findet sich aber nur die "beispielhafte Maßnahme", das der
Grundversorgungstarif auf Ökostrom umgestellt wird.

Punkt 8: Hat viel Potenzial. Momentan fehlt es Augsburgs Verwaltung erheblich
an Stellen und Haushaltsmitteln, um alleine schon die (ungenügenden) bisherigen
Klimaschutzziele erfüllen zu können. Etwa gibt es nur einen einzigen
Radbeauftragten, und dieser steht in der Hierarchie an vierter Stelle. Hier
könnte die Verwaltung nun angeben, welche Stellen sie in welchen Bereichen
benötigt. In einer Folgeentscheidung könnten diese dann bewilligt werden.

Punkt 9: Viele der sich engagierenden Schüler\*innen sind die ewige Vertröstung
auf Gespräche, in denen die Wichtigkeit von Klimaschutz betont und das
Engagement der Jugendlich gelobt wird, leid. Wir benötigen angemessene Ziele
und wirksame Maßnahmen. Sofern Finanzmittel nicht ausreichen, ist deren
Einwerbung mit größter Priorität zu verfolgen. Trotzdem hat ein geordnetes
Dialogformat das Potenzial, dem am Maßstab des wissenschaftlich Nötigen viel zu
langsam ablaufenden Prozess einer sozial-ökologischen Transformation sinnvoll
zu begleiten. Wir freuen uns auf die weiteren Gespräche mit der Stadt.

Punkt 10: Sehr sehr gut!


## Hinweis in Sachen Aberkennung des Versammlungsstatus

Das Ordnungsamt nahm gestern [unser Angebot](/pages/Pressemitteilungen/2020-12-14-Versammlungsverbot.html) einer
außergerichtlichen Lösung nicht an.

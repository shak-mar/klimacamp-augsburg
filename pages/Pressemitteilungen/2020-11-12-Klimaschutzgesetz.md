---
layout: page
title:  "12.11.2020: Friedlicher ziviler Ungehorsam als Reaktion auf nun beschlossenes verantwortungsloses Klimaschutzgesetz"
date:   2020-11-12 16:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 26
---

*Kurzmeldung vom Augsburger Klimacamp am 12. November 2020*

# Friedlicher ziviler Ungehorsam als Reaktion auf nun beschlossenes verantwortungsloses Klimaschutzgesetz

Heute winkte die Regierungskoalition im bayerischen Landtag
das von ihr so bezeichnete "Klimaschutzgesetz" durch. Im Vorfeld
meldeten zahlreiche Umweltverbände sowie alle in den Landtag geladenen
Expert\*innen schwerste Bedenken an -- mit Ausnahme der Personen, die von
CSU und AfD entsendet wurden.

"Dem Gesetz fehlt es an Verbindlichkeit und an Kontrollstrukturen. Es schreibt
keinerlei Treibhausgasbudgets für die einzelnen Sektoren vor. Das Gesetz ist
völlig realitätsfern", kritisiert Klimacamperin Stefanie Bauer (18). "Mit
welchem Recht nimmt es sich unsere Landesregierung heraus, sich über das
demokratisch beschlossene Pariser Klimaabkommen zu stellen?", fragt Bauers
Mitstreiterin Anouk Zenetti (13).

"Genug ist genug. Herr Söder und Frau Weber müssen verstehen, dass sie
die Lebensgrundlage der jungen Menschen sowie die Wirtschaft im Jahr
2040 nicht ständig hintenan stellen können. Um darauf hinzuweisen, wird
es ab sofort mehrere Aktionen des friedlichen zivilen Ungehorsams
geben."

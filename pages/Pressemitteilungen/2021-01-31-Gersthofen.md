---
layout: page
title:  "31.01.2021: Pressemitteilung von unabhängigen Aktivist*innen zu Baumbesetzung in Gersthofen"
date:   2020-01-31 16:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 35
---

*Pressemitteilung am 31. Januar 2021*

# Klimagerechtigkeitsaktivist\*innen besetzen Bäume in Gersthofen

Sperrfrist Montag (1.2.2021) 6:00 Uhr
{: .label .label-red }

**[ Diese Pressemitteilung gab nicht das Klimacamp, sondern eine Gruppe
unabhängiger Aktivist\*innen 
heraus. Sie findet sich auf dieser Seite wegen der thematischen Relevanz der
Bequemlichkeit halber trotzdem. ]**

Am morgigen Montag (1.2.2021) werden ab 6:00 Uhr in der Früh
Klimagerechtigkeitsaktivist\*innen verschiedener Initiativen die Bäume in
Gersthofen, die im Zuge der Bauarbeiten am Parkplatz
Schubertstraße/Tiefenbacherstraße gefällt werden sollen, besetzen, um so
die Fällung zu verhindern.

"Die Stadt täuschte im Vorfeld ganz bewusst ihre Bürger\*innen mit einer
irreführenden Fotomontage", erklärt Mathematiker Ingo Blechschmidt (32),
der an der Aktion beteiligt ist. Blechschmidt bezieht sich damit auf
einen Hinweis einer Gersthofer Bürgerin in der AZ [1]. "Natürlich gibt
es durchaus einige wenige sinnvolle Gründe, Bäume zu fällen. Wir haben
aber kein Vertrauen in die Stadt, dass sie die Baumfällungen mit der
nötigen Ernsthaftigkeit prüfte und sorgfältig Alternativen abwog."

"Dass Bäume immer noch von unseren Regierungen kontinuierlich
geringschätzt werden, fügt sich leider in ein immer präsentes Muster
ein", konstatiert Blechschmidts Mitstreiterin Ute Grathwohl (47). "Alle
Politiker\*innen behaupten fortwährend, Klimaschutz ernst zu nehmen und
das Engagement der Jugend wertzuschätzen, aber trotzdem ergreifen sie
nicht dringend nötige Klimagerechtigkeitsmaßnahmen. Dabei würden diese
Maßnahmen sogar unabhängig von der Klimakrise unser aller Lebensqualität
erhöhen und gegen die wachsende Kluft zwischen Arm und Reich steuern."

Die Aktivist\*innen fordern, dass die Stadt ein wissenschaftliches und
unabhängiges Gutachten vorlegt, das die Notwendigkeit der Baumfällungen
nachweist und sich bei ihren Bürger\*innen für ihre Täuschung förmlich
entschuldigt und künftig dem Schutz unserer Lebensgrundlagen einen
höheren Stellenwert beimisst.

[1] https://www.augsburger-allgemeine.de/augsburg-land/Wie-viele-Baeume-fallen-fuer-den-Gersthofer-Gymnasium-Neubau-id58985446.html

## Hinweis

Es kann kurzfristig zu Änderungen im Ablauf kommen.

# Ergänzung am 1.2.2021: Partielle Gegendarstellung und Bilanz zur heutigen Baumbesetzung in Gersthofen

1. Verantwortlich für das Baumdesaster ist nicht die Stadt Gersthofen, sondern
der Landkreis. Das stellten wir in der vorherigen Pressemitteilung zur Aktion
falsch dar. "Der Fehler war zu oberflächlicher Recherche geschuldet und tut uns
leid", so Ingo Blechschmidt (32). "Wir schätzen diesbezüglich Bürgermeister
Michael Wörle, der auf uns zu kam und mit dem ein vertrauensvolles Gespräch
möglich war."

2. Die in Aussicht gestellten Baumneupflanzungen werden erst in vielen Jahren
vergleichbar viel CO₂ binden können wie der aktuelle Baumbestand. "Doch der
eigentliche Skandal liegt darin, dass der Landkreis das neue Schulgebäude nicht
in klimaneutraler Bauweise errichtet (Passivhausstandard), sondern im
veralteten KfW 55", so Grathwohl. "Durch das Bauverhaben trägt der Landkreis
also aktiv zur Erdaufheizung bei. Dabei sollte der Landkreis doch in Sachen
Klimagerechtigkeit eine Vorbildrolle einnehmen!"

3. Anwohner\*innen im Rentenalter schlossen sich unserem Protest an. Dabei
stellte sich heraus, dass die Rechtslage zur Fällung keineswegs gesichert war.
"Einem Anwohner wurde zuvor zugesichert, dass ohne Fakten zu schaffen das
Verwaltungsgericht angerufen werden könne und die Fällung bis mindestens
Mittwoch ausgesetzt werden würde", erklärt Klimagerechtigkeitsaktivistin Ute
Grathwohl (47). Diese Zusage wurde trotz permanenter Hinweise gebrochen. Dem
Anwohner wurde unverständlicherweise ein Bußgeld in Aussicht gestellt.

4. Durch die Baumbesetzung konnten heute die Fällarbeiten weitgehend verhindert
werden. Nach etwa einer Stunde wurden sie für den Tag vorzeitig beendet.

## Fotos und Videos zur freien Verwendung (Urheberin Ute Grathwohl)

https://www.speicherleck.de/iblech/stuff/gersthofen-2021-02-01/

## Hinweis

Bei Interesse stellen wir gerne den Kontakt zum Anwohner, der die Zusage der
Fällungsaussetzung hatte, her.

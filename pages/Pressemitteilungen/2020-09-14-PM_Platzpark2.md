---
layout: page
title:  "14.09.2020: Schüler*innen erstatten Strafanzeige gegen Stadt"
date:   2020-09-14 12:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 16
---

*Pressemitteilung vom Augsburger Klimacamp am 14. September 2020*

# Schüler\*innen erstatten Strafanzeige gegen Stadt

Vergangene Woche platzierten Klimagerechtigkeitsaktivist\*innen in der
Maxstraße einen Platzpark, ein Hochbeet mit integrierten
Fahrradstellplätzen. Schon einen Tag später wurde der Platzpark von der
Stadt abgebaut. Wegen dieser Sachbeschädigung erstatteten die
Schüler\*innen des Augsburger Klimacamps nun Strafanzeige gegen die
Stadt.

„Der Platzpark lud zum Verweilen ein“, erklärt Aktivistin Sarah Bauer (16).
„Die Blumen waren ein Beitrag gegen das Insektensterben und die
drei integrierten Fahrradständer trugen zum Gelingen der 2012 einstimmig
vom Stadtrat beschlossenen Vision 'Fahrradstadt 2020' bei.“

Baureferent Gerd Merkle (CSU) ließ den Platzpark aber schon am folgenden
Tag abbauen. „Wer alle Rechte einer Demokratie für sich in Anspruch
nimmt, muss sich auch mit den Pflichten auseinandersetzen und diese
einhalten“, gab Merkle in einem Interview der AZ bekannt [1].

„Zu diesen Pflichten gehört aber auch die Pflicht (Art. 20a GG), für die
künftigen Generationen die Lebensgrundlagen zu schützen“, erwidert
Aktivistin Luzia Menacher (19). „Wir fragen uns, mit welchem Recht sich
die Stadt Augsburg herausnimmt, das Pariser Klimaschutzabkommen um das
dreifache zu brechen.“ Der Koalitionsvertrag sieht vor, dass bis 2050
noch 34 Millionen Tonnen CO₂ emittiert werden sollen. Der Stadt stehen
aber, um ihren Anteil am Pariser Klimaabkommen einzuhalten, nur 11
Millionen Tonnen zu. Dieses Budget soll 2025, noch während der jetzigen
Legislatur, vollständig aufgebraucht werden.

„Zudem ist die Demontage unseres Platzparks Sachbeschädigung. Das
Hochbeet wurde von mehreren Schüler\*innen im Lauf von drei Tagen gebaut
und kostete etwa 250 Euro an Material. Es fehlte zwar der Parkschein,
aber werden falsch parkende Autos auch auseinandergebaut?“, ergänzt
Bauer. Die Stadt hätte mit den Aktivist\*innen Kontakt aufnehmen können,
sie seien ja rund um die Uhr im Klimacamp. „Zu einer Kontaktaufnahme kam
es aber nicht. Wegen der Sachbeschädigung erstatteten wir heute
Strafanzeige bei der Polizei.“

„Wir erwarten von unserer Stadt, dass sie die Klimakrise ernst nimmt.
Stattdessen verwenden Politiker\*innen ihre Zeit kindisch, um auf die
symbolische Aktion von jungen Menschen unverhältnismäßig zu reagieren.“,
so Menacher. Solch ein Verhalten zeige, dass Augsburg noch lange nicht
eine klimafreundliche und erst recht nicht die klimafreundlichste
Metropole Bayerns ist.

Dass es auch anders geht, beweisen Städte wie Freiburg. Dort ging die
Stadt nach einer ähnlichen Aktion eine Kooperation mit den
Klimaaktivist\*innen ein, seitdem stehen mehrere Platzparks im
Stadtgebiet [2].

[1] https://augsburger-allgemeine.de/augsburg/Die-Stadt-entfernt-das-Hochbeet-der-Klimacamp-Aktivisten-an-der-Maxstrasse-id58101506.html  
[2] https://extinctionrebellion.de/og/freiburg/und-sonst-so/platzpark-update-was-dann-geschah-und-noch-geschehen-soll/

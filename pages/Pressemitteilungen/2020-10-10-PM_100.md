---
layout: page
title:  "10.10.2020: Gemischte Gefühle bei Diskussionsrunde anlässlich des 100-tägigen Jubiläums des Klimacamps"
date:   2020-10-10 23:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 22
---

*Pressemitteilung vom Augsburger Klimacamp am 10. Oktober 2020*

# Gemischte Gefühle bei Diskussionsrunde anlässlich des 100-tägigen Jubiläums des Klimacamps

**[Fotogalerie](https://www.speicherleck.de/iblech/stuff/.klimacamp-100/web)**

Der Einladung der Augsburger Klimacamper\*innen zur Diskussionsrunde
anlässlich ihres 100-tägigen Jubliäums folgten knapp 80 Menschen,
darunter Vertreter\*innen von AiB, CSU, DIE LINKE, Die PARTEI, FDP, FW,
Grüne, ÖDP und SPD.

"Alle Lokalpolitiker\*innen, die wir eingeladen haben, folgten unserer
Einladung", freut sich Aktivistin Paula Stoffels (18). "Auch freuten wir
uns über die zahlreichen positiven und konstruktiven Beiträge, die
diverse Gesprächsteilnehmer\*innen tätigten. Das zeigt uns, dass die
Forderungen der Wissenschaft, denen wir Gehör verschaffen, ihren Weg in
die politischen Strukturen geschafft haben und dort ernst genommen
werden."

"Allerdings gab es von beiden Regierungsparteien CSU und Grüne für uns
enttäuschende Redebeiträge", so Stoffels weiter. "CSU-Stadtrat Max
Weinkamm erklärte, er verstehe die Verzweiflung der jungen
Aktivist\*innen des Klimacamps nicht. Das zeigt, dass die Schwere und die
akute, bedrohliche Situation der Klimakrise leider noch nicht von allen
wichtigen Entscheidungsträger\*innen verstanden wird."

Seit Jahrzehnten ist bekannt, dass der Weltgemeinschaft nur noch ein
begrenztes CO₂-Budget zusteht. Die Treibhausgasemissionen müssen
weltweit drastisch reduziert werden, um mit einer ⅔-Wahrscheinlichkeit
die Erderhitzung auf 1,5 Grad zu beschränken und die Kippelemente im
Klimasystem nicht auszulösen. Dazu bekannte sich die Weltgemeinschaft
2015 im Pariser Klimaabkommen. Da die weltweiten Emissionen allerdings
Jahr für Jahr steigen, sind viele Menschen zutiefst besorgt und gerade
Aktivist\*innen auch oft verzweifelt.

Im Hinblick auf die Möglichkeit für Regierungen, Schulden aufzunehmen,
um dringend nötige Klimagerechtigkeitsmaßnahmen zu ergreifen, erwiderte
Max Weinkamm, dass er seinen Kindern und Enkeln gewiss keinen
Schuldenberg hinterlassen wolle. Stoffels Mitstreiter Niclas Nickl (17)
gibt zu Bedenken, dass "jedes Zehntelgrad Erderhitzung ein
unvorstellbarer Schuldenberg ist, den wir allen zukünftigen Generationen
hinterlassen. Menschliches Leid ist nicht in Euro bezifferbar; jeder
Euro, der jetzt nicht in Klimagerechtigkeitsmaßnahmen investiert wird,
wird in den nächsten Jahren und Jahrzehnten ein Vielfaches an Kosten
verursachen."

Ebenfalls stieß eine Aussage von Stefan Wagner (Grüne) auf
Unverständnis. "Die Grünen gelten landläufig als die Umweltpartei
schlechthin und wir sind dankbar für ihren langjährigen Einsatz. Jedoch
sind wir erschüttert über das Vorhaben der grünen Fraktion, keinerlei
Maßnahmen zur Stärkung der Klimagerechtigkeit in Augsburg zu unternehmen
bevor ein Gesamtplan aller notwendigen Maßnahmen vorliegt", so Nickl.
Dieser Plan solle erst Frühjahr nächsten Jahres fertiggestellt sein.

Die Aktivist\*innen des Augsburger Klimacamps stimmen zu, dass ein
solcher durchdachter systematischer Plan zur CO₂-Neutralität wichtig ist
und auf jeden Fall ausgearbeitet werden muss. "Dennoch darf er nicht
davon abhalten, parallel zur Planausarbeitung jetzt schon Maßnahmen, die
sehr wirksam, erforscht und leicht umsetzbar sind, zu ergreifen",
fordert Nickl.

## Teilnehmende Politiker\*innen

* Philipp Höfl (Umweltreferent der FDP-Landtagsfraktion)
* Peter Hummel (FW)
* Anja Klingelhöfer (Die PARTEI)
* Gregor Lang (Sozialfraktion)
* Bruno Marcon (Augsburg in Bürgerhand)
* Christian Pettinger (ÖDP)
* Lars Vollmar (FDP)
* Stefan Wagner (Grüne)
* Max Weinkamm (CSU)

## Hinweis zum weiteren Abendprogramm

Um 21:00 Uhr begann wie angekündigt die 24-Stunden-Besetzung von einer der
Linden in der Fuggerstraße (der nächsten am Theater), um auf die
Trauerweide von Frau Demmelmair, den Fuggerlinden und den Bäumen im
Dannenröder Wald aufmerksam zu machen. Diese stehen alle auf der
Abschussliste der Regierungen. Die Fotogalerie enthält ein Foto der
Besetzung.

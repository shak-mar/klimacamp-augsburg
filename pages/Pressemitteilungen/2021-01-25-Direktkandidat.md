---
layout: page
title:  "25.01.2021: Pressemitteilung von Fridays for Future Augsburg zur Bundestagswahl"
date:   2020-01-25 23:30:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 34
---

*Pressemitteilung von Fridays for Future Augsburg am 25. Januar 2021*

# Augsburger Klimagerechtigkeitsaktivist nimmt ÖDP-Angebot zur Aufstellung als parteiloser Direktkandidat bei der Bundestagswahl an

**[ Diese Pressemitteilung gab nicht das Klimacamp, sondern FFF Augsburg
heraus. Sie findet sich auf dieser Seite wegen der thematischen Relevanz der
Bequemlichkeit halber trotzdem. ]**

Der seit Beginn von Fridays for Future aktive Augsburger
Klimagerechtigkeitsaktivist Alexander Mai (24) stellt sich bei der
kommenden Bundestagswahl als parteiloser Direktkandidat für den
Wahlkreis Augsburg-Stadt auf. Diese Möglichkeit bietet ihm die ÖDP
Augsburg, die ihren Platz der Klimagerechtigkeitsbewegung zur Verfügung
stellt. Das ungewöhnliche an der Kandidatur ist, dass Mai weder
ÖDP-Mitglied ist noch sonst einer Partei angehört oder beitreten wird.
"Ich hatte nie und habe nicht das Ziel, Parteipolitiker zu werden. Wir
Aktivist\*innen prangern parteiübergreifend Missstände in der Politik
an", bekräftigt der parteilose Klimagerechtigkeitsaktivist und
Mathematikstudent Alex Mai.

Klimacamper Ingo Blechschmidt (32) freut sich sehr: "Die ÖDP Augsburg
tat sich bei uns immer wieder besonders als Partei hervor, indem sie
schon lange Aktivismus und Bürger\*innenbeteiligung in Augsburg
unterstützte. Wir möchten den Augsburger\*innen mit Alex' Kandidatur die
Möglichkeit geben, eine authentische und unvoreingenommene Person mit
der Repräsentation Augsburgs zu beauftragen. Davon abgesehen ist unser
übergeordnetes Ziel für die Bundestagswahl, dass alle antretenden
demokratischen Parteien unabhängig von ihrer Einordnung im politischen
Spektrum die Einhaltung des demokratisch beschlossenen Pariser
Klimaabkommens durch sozial gerechte Maßnahmen in ihren Grundsätzen
verankern. Die Bundestagswahl wird Klimawahl!"

Nahezu alle deutschen Politiker\*innen betonen, wie wichtig ihnen
Klimaschutz sei und wie sehr sie das Engagement der Jugend schätzten.
"Trotzdem zeigt Deutschland bis heute gar keine Verantwortung beim Klima
und trägt weiter dazu bei, eine lebenswerte Zukunft unerreichbar werden
zu lassen. Stattdessen sollten wir wieder eine Vorreiterrolle
einnehmen", prangert Schülerin Janika Pondorf (16) an. "Unsere Regierung
spielt damit mit unser aller Zukunft!" Um die Politik dazu zu bewegen,
Klimagerechtigkeit so ernst zu nehmen, dass Deutschland das demokratisch
beschlossene Pariser Klimaabkommen doch noch einhält, versuchten
Augsburgs Klimagerechtigkeitsaktivist\*innen schon Vieles: zum Beispiel
Gespräche mit Politiker\*innen, Bürger\*innenbegehren, Demonstrationen,
Müllsammeln, Vorträge, Workshops, Diskussionsrunden, dauerhafte
Klimacamps oder liebevollen zivilen Ungehorsam.

"Doch das alles brachte uns vor allem leere Versprechen ein", so Pondorf
weiter. "Und wenn doch mal von Klimaschutz die Rede ist, dann oft auf
Verantwortung von Privatpersonen und ohne Beachtung sozialer Belange",
ergänzt Mai. "Für uns ist Klimagerechtigkeit aber untrennbar mit
sozialer Gerechtigkeit verbunden. Wir sind etwa strikt dagegen,
Benzinpreise ohne sozialen Ausgleich zu erhöhen". Das benachteilige nur
ärmere Menschen, die sowieso schon stärker von der Klimakrise betroffen
sind und zugleich weniger zu ihr beitragen. "Wir setzen stattdessen
unter anderem auf beachtlichen Ausbau und Vergünstigung von Bus und
Tram, sodass alle Menschen komfortabel und zukunftstauglich von A nach B
kommen können." Als Nebenwirkung würde das die Innenstädte sogar noch
lebenswerter und gemütlicher machen, Unorte wie aktuell die Karlstraße
würde es nach einer Mobilitätswende nicht mehr geben. Menschen, die
wirklich auf ihr Auto angewiesen sind, hätten endlich freie Straßen.
"Und Arbeitsplätze wären für die Zukunft gesichert. Unsere Wirtschaft
könnte durch die zahlreichen Strukturveränderungen regelrecht
aufblühen."

Mai folgt mit seiner Kandidatur auch dem andauernden Verlangen von
Bürger\*innen, die in Gesprächen oft betonen: "Ihr solltet in die Politik
gehen!". Dabei ist für die Aktivist\*innen klar, dass das nicht ihre
eigentliche Aufgabe ist. "Spätestens wenn es Konflikte bei finanziellen
Interessen gibt, braucht die Politik Druck aus der Gesellschaft",
erklärt Mai. Pondorf führt aus: "Dafür sind wir da. Die regelrechte
Explosion an bundesweitem Aktivismus begreifen wir aber auch als Wunsch
der breiten Bevölkerung, dass unbefangene, frische und engagierte
Menschen unsere Politik gestalten."

Mai: "Wenn ich gewählt werden sollte, werde ich meine ganze Energie in
meine Verantwortung als Abgeordneter stecken. Nebentätigkeiten wird es
bei mir keine geben -- und das sollten wir auch von anderen Abgeordneten
erwarten". "Durch meinen Hintergrund als Augsburger Stadtkind einer
russischen Bauern- und Arbeiterfamilie, Mitarbeit in meiner Jugend im
Flüchtlingshelferkreis Pfersee, als ehrenamtlicher Rettungsschwimmer und
Sanitäter in der DLRG, bis heute seit mehr als zehn Jahren als führende
Rolle in der freien Sportgruppe Parkour-Augsburg und als
Mathematikstudent liegen mir auch Themen wie Bildung, Digitalisierung,
Sport und Erholung, ehrenamtlicher Einsatz, Integration, Transparenz und
Teilhabe als Betroffener am Herzen."

Blechschmidt freut sich nicht nur auf die Wahl, sondern auch auf den
Wahlkampf: "Alex wird bei Podiumsdiskussionen die Erwartungen an die
anderen Kandidat\*innen deutlich erhöhen. Augsburg wird erleben, wie viel
mehr möglich ist!"

## Hinweis

Das von Fridays for Future Augsburg initiierte Klimacamp steht seit nun
mehr als 200 Tagen durchgehend neben dem Rathaus. Dort können alle
Interessierten mit Klimagerechtigkeitsaktivist\*innen ins Gespräch
kommen. Mai ist dort immer donnerstags von 12 bis 20 Uhr und auch an
vielen anderen Tagen.

---
layout: page
title:  "04.09.2020: Die ersten Erfolge aus dem Klimacamp"
date:   2020-09-04 07:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 14
---

*Pressemitteilung vom Augsburger Klimacamp am 4. September 2020*

# Die ersten Erfolge aus dem Klimacamp

Seit nun über zwei Monaten campen Klimaaktivist\*innen aus Augsburg und
ganz Deutschland neben dem Augsburger Rathaus und protestieren für eine
klimagerechte Stadt und gegen das Kohleausstiegsgesetz. Nun können sie
von ersten kleinen Erfolgen und maßloser Enttäuschung berichten. 

Vor 65 Tagen wurde das damals noch eher spärliche Klimacamp
errichtet. Jetzt sieht es vor Ort schon ganz anders aus. Große Zelte,
selbst gebaute Bänke, Hochbeete, Pavillons, bunte Flaggen und
meinungsstarke Banner prägen nun den Fischmarkt. Doch nicht nur
äußerlich hat sich seit Tag 1 viel verändert. Die Aktivist\*innen
kamen in den letzten zwei Monaten gezielt immer wieder in das Gespräch
mit Wirtschaft, Politik und Gesellschaft. Die Resultate fielen sehr
unterschiedlich aus. Während in der Gesellschaft und in der Wirtschaft
die Gespräche positiv, zielorientiert mit viel Verständnis liefen, fiel
bei den häufigen Gesprächen mit der CSU auf, dass viel von oben herab
geredet wurde. Bei den Grünen ist überraschend, dass es teilweise am
grundlegenden Fachwissen fehlte.

„Bei Gesprächen mit der Stadtregierung wurde deutlich, dass die CSU die
Klimakrise nicht als echte Krise erkennt und die Grünen lange nicht so
`grün´ sind wie es oft scheint. Zudem fehlt bei fast allen Parteien das
nötige Fachwissen, um das Ausmaß und die Verantwortung ihrer
Entscheidung zu verstehen und ihnen gerecht zu werden“, sagte Sarah
Bauer (16) in einer Reflektionsrunde im Klimacamp. 

Dennoch ist klar, dass vom Klimacamp aus weiterhin viele, auch wenn
frustrierende, Gespräche geführt werden müssen. Die Motivation im
Protestcamp besteht weiterhin, auch deshalb, weil es nichts anderes zu
verlieren gibt als die eigene Zukunft. Bestärkt wird dies auch durch
den spürbaren Rückhalt in nicht nur Augsburgs Gesellschaft und die
anhaltende Berichterstattung. 

Wie auch sonst in der Klimagerechtigkeitsbewegung ist die öffentliche
Meinungskundgabe zentraler Teil der vielen Aktionen. Das Protestcamp hat
seit dem ersten Tag die Aufmerksamkeit der Augsburger Bevölkerung und
Politik, sodass seit 65 Tagen ein dauerhaften Diskurs über die
notwendigen Maßnahmen zu Klimagerechtigkeit aufrecht gehalten wird.
„Alle, die unseren Protest in dieser Form kritisieren, vergessen oft,
dass ohne die langjährige konstante Arbeit der Aktivist\*innen nicht über
die Klimakrise diskutiert oder sie sogar vollständig in Verschwiegenheit
geraten würde. Das heißt, dass die breite Masse und die Kritiker\*innen
ohne uns gar nicht über die Klimakrise und ihre Folgen aufgeklärt wären
und jetzt, auch in Form von Kritik, nicht darüber nachdenken würden“,
stellte Leon Ueberall (17) fest.

Ein kleiner Erfolg ist auch der gemeinsame
Geschäftsordnungsänderungsantrag von FW, LINKE, ÖDP, SPD und V-Partei.
Die jetzige Satzung des Stadtrats sieht vor, dass Anträge von
Stadträt\*innen prinzipiell drei Monate lang vertagt werden dürfen, bevor
sie diskutiert werden müssen. Der Anderungsantrag möchte diese Frist für
klimarelevante Anträge auf einen Monat reduzieren. „Die Idee zu
diesem Änderungsantrag kam in einer Gesprächsrunde im Klimacamp auf. Er
spiegelt wieder, dass die Klimakrise nicht auf unsere
Bearbeitungsfristen wartet.“, freut sich Ueberall.

„Das alles können wir als kleine Erfolge sehen. Sie wird uns allerdings
nicht weiter bringen, solange die Stadt Augsburg nicht versteht,
wie groß das Ausmaß der Krise ist, dass Augsburg mit seinem jetzigen
Handeln Teil des riesigen Problems ist und dass Augsburg lokal die
Klimakrise mit aufhalten kann und muss“, empört sich Bauer.

Eines steht bei den Klimaaktivist\*innen aus dem Protestcamp jedoch
fest: Der friedliche Einsatz um die menschlichen Lebensgrundlagen muss
weiter gehen. 

„Das Protestcamp motiviert uns alle einzigartig, dennoch ist es
traurig, dass ich und viele andere von weit her anreisen müssen, um der
Stadt Augsburg bewusst zu machen, dass ihre Entscheidungen bundesweite
und globale Auswirkungen haben werden. In Augsburg ist, wie in vielen
anderen deutschen Städten, noch nicht das Verständnis für die eigene
Beteiligung am aktuellen klimapolitischen Versagen vorhanden. Ich sehe
in Augsburg allerdings viel nicht genutztes Potenzial, um Vorbild für
andere Kommunen weltweit zu werden“, sagte Max Frank aus dem 200 km
entfernten Eppingen.

Das Augsburger Klimacamp ist mittlerweile zum Vorbild für viele andere
Städte geworden. Klimacamps sind in Dresden, Halle, Hamburg, und München
bereits entstanden und in Bamberg, Lübeck, Nürnberg und Ulm in Planung.

## Schlussbemerkung: Für die Zukunft sind zahlreiche Aktionen geplant.

1. Mit dem Ende der Sommerpause beginnen wieder unsere
   Politiker\*innengespräche. Wir möchten Einzelgespräche mit allen
   Stadträt\*innen führen.
2. Auch planen wir mehrere Aktionen, die noch mal auf das Thema
   Fahrradstadt hinweisen sollen. Am 11.9. werden wir von 7:00 Uhr bis 8:00
   Uhr in der Früh wieder in der Hermanstraße einen Popup-Fahrradweg
   errichten — allerdings nicht wie die letzten drei Male nur die 50 Meter
   vor der Ampel, sondern bis fast ganz hinten zur Brücke. Möglicherweise
   machen wir zu Schulbeginn am Dienstag auch eine Aktion in der Hallstraße
   am Holbein-Gymnasium (die soll ja für Autos ausgebaut werden, anstatt
   dass sie verkehrsberuhigt wird). Eine Aktion zur Karlstraße ist auch in
   Planung. Ferner möchten wir gerne am 20.9. die Kidical Mass, die bunte
   Fahrraddemo für alle, ausrichten.
3. Diverse Wissenschaftler\*innen folgten unserer Einladung, ihre
   Forschung bei uns im Camp zu präsentieren. Dazu sind jeweils auch unsere
   Stadtpolitiker\*innen geladen. Den Auftakt der neuen Vortragsreihe machte
   vor zwei Tagen Prof. Dr. Matthias Schmidt (Lehrstuhlinhaber
   Humangeographie und Transformationsforschung) zum Thema „Fluchtursache
   Klimawandel?“. Am heutigen Freitag stellen die beiden Autor\*innen einer
   [kürzlich veröffentlichten
   Studie](https://taz.de/Was-Lebensmittel-kosten-muessten/!5710701/) ihre Arbeit
   vor: „Was kosten Lebensmittel wirklich?“. Am 15.9. wird Robert Kugler
   vom Naturwissenschaftlichen Verein Schwaben über Artenschutz referieren.
   Und am 22.9. oder 23.9. kommt eine Person vom Landesamt für Umwelt.
4. Am 25.9. ist weltweiter Fridays-for-Future-Globalstreik, der erste
   nach der Corona-Zwangspause. In Augsburg wird es voraussichtlich um
   17:00 Uhr auf dem Rathausplatz losgehen. Es wird eine Fahrraddemo, deren
   Route auch über die B17 verlaufen soll. Details sind gerade in Planung
   und in Abstimmung mit den relevanten Behörden.

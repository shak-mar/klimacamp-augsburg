---
layout: page
title:  "23.4.2021: Klimagerechtigkeitsaktivist*innen verbünden sich für den Erhalt des Meitinger Lohwalds"
date:   2021-04-23 07:30:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 41
---

*Pressemitteilung von Wald statt Stahl am 23. April 2021*

# Klimagerechtigkeitsaktivist\*innen verbünden sich für den Erhalt des Meitinger Lohwalds

Sperrfrist Samstag (24.4.2021) 8:00 Uhr
{: .label .label-red }

**[ Diese Pressemitteilung gab nicht das Klimacamp, sondern Wald statt Stahl
heraus. Sie findet sich auf dieser Seite wegen der thematischen Relevanz der
Bequemlichkeit halber trotzdem. ]**

Am Samstagmorgen, den 24.04., hingen Klimagerechtigkeitsaktivist\*innen des
lokalen Aktionsbündnis "Wald statt Stahl" erneut ein Banner für den Erhalt des
Lohwalds in Meitingen auf, welcher für eine Erweiterung der Lech-Stahlwerke
(LSW) gerodet werden soll. Das ist bereits das vierte Banner des
Aktionsbündnisses (AZ berichtete [1]), welches neuerdings nun Teil des
"Bannwald-Bündnis unterer Lech", sowie Mitunterzeichner der "Meitinger
Bannwald-Erklärung" ist. Das neue Banner wird nich mehr im Schlosspark, sondern
direkt am Rathaus angebracht werden.

Zusammen mit den anderen Unterzeichnenden wollen sie den gesetzlich als
Bannwald geschützten Wald erhalten. "Wir müssen jetzt geschlossen agieren, und
zeigen, dass alle Mitglieder dieses Bündnisses -- auch die kletternden --
gemeinsam die Rodung verhindern werden!", so Markus Eckstein (34), Vorstand der
BI Lech-Schmuttertal e.V., sowie einer der Mitinitiator\*innen des
Bannwald-Bündnis.

Nachdem die Aktivist\*innen von Wald statt Stahl letzte Woche vor dem Büro des
Gemeinderats und Landtagsabgeordneten Dr. Fabian Mehring einen sogenannten
"Platzpark" platzierten, lud dieser sie über Twitter zu sich ins Büro ein [2].

"Wir danken Herrn Mehring für die nette und humorvolle Einladung, aber wollen
einem einzelnen Gemeinderat nicht die Chance geben, sich zu profilieren", so
Gwendolyn Rautenberg (19) von Wald statt Stahl. "Nachdem circa 600
demonstrierende Menschen vor dem Stahlwerk keine Reaktion hervorriefen,
dafür ziviler Ungehorsam aber zu einer Gesprächseinladung führte,
bleibt uns nichts anderes übrig, als noch mehr und noch ungehorsamere Aktionen
folgen zu lassen, um damit eine Einladung zu erhalten, vor dem gesamten
Gemeinderat sprechen zu können."

"Die Erfahrung der Mitglieder des Bannwald-Bündnisses lässt allerdings wenig
von solch einem Gespräch erhoffen", so Rautenberg weiter. "Seit Jahren reden
wir schon mit den Politiker\*innen in Meitingen, aber stoßen mit unserem
Anliegen jedes mal gegen eine Wand", erklärt Eckstein. Der Forststudent und
angehende Förster Nico Kleitsch (21) weist darauf hin, dass die Aktionen von
Wald statt Stahl erst mit dem Erhalt oder gar der Erweiterung des Bannwaldes
enden, nicht mit einem Gespräch vor dem Gemeinderat: "Es kann nicht sein, dass
ein einzelner Mensch wie Max Aicher (81), Besitzer des Werks und einer der
reichsten Menschen Deutschlands, nicht nur die Macht über die Arbeitsplätze der
Stahlwerks-Arbeiter\*innen hat, sondern auch über Lärmbelastung, intakte Natur
und CO2-speichernde Wälder, und somit auch über unserer aller Zukunft."

[1] https://m.augsburger-allgemeine.de/augsburg-land/Aktivisten-setzen-erneut-Zeichen-gegen-Lohwald-Rodung-id59472581.html<br>
[2] https://m.augsburger-allgemeine.de/augsburg-land/Warum-Klima-Aktivisten-Fabian-Mehring-ein-Hochbeet-schenken-id59492496.html

## Fotos zur freien Verwendung

Hier herunterladen: https://www.speicherleck.de/iblech/stuff/.lw4 (wird im
Laufe des Vormittags gefüllt)

Falls benötigt, Quellenangabe "Wald statt Stahl". Von unserer Seite aus ist
keine Quellenangabe nötig.

## Über das Aktionsbündnis

"Wald statt Stahl" ist ein Zusammenschluss von
Klimagerechtigkeitsaktivist\*innen diverser Bewegungen aus Meitingen, Augsburg
und weiteren Städten aus der Region. Sie eint das Ziel, mit Aktionen und
zivilem Ungehorsam den Meitinger Lohwald zu erhalten und seine Erweiterung
einzufordern. Am 9.4.2021 fand die erste Aktion des Aktionsbündnisses statt,
weitere werden folgen. "Wald statt Stahl" agiert unabhängig vom
Bannwald-Bündnis Unterer Lech, welches sich ebenfalls für den Erhalt des
Lohwalds einsetzt.


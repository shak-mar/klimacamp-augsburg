---
layout: page
title:  "30.11.2020: Klimacamp beschenkt Fans mit Informationen – Adventskalender voller spannender Videos geplant"
date:   2020-11-30 11:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 28
---

*Pressemitteilung vom Augsburger Klimacamp am 30. November 2020*

# Klimacamp beschenkt Fans mit Informationen — Adventskalender voller spannender Videos geplant

Vom 1.12. bis zum 24.12. gibt es täglich neu veröffentlichte Videos vom
Augsburger Klimacamp. Die Inhalte sind noch am Entstehen und reichen von
Erklärungen typischer Missverständnisse zum Thema Klimagerechtigkeit, über
Live-Musik, Gastbeiträge von Stadträt\*innen, Erfahrungen und
Beteiligungsmöglichkeiten im Stadtrat, bis hin zu Podcasts über nun fast sechs
Monate des Protestcamps. In einigen Fällen gibt es auch begleitendes Material
zum Weiterlesen und Recherchieren.

Die Inhalte der Adventskalendertürchen werden jeweils in kurzen Clips über den
Instagram-Account des Klimacamps (@klimacamp) angekündigt und vorgestellt. Auch
auf der Klimacamp-Website augsburg.klimacamp.eu wird der Adventskalender mit
seinen Inhalten präsentiert. Die vollen Videos werden auf dem YouTube-Kanal
"Fridays for Future Augsburg" zur Verfügung gestellt.

Seit sich Augsburg im Oktober zum Corona-Hotspot entwickelt hat, reduzierten
die Klimacamper\*innen freiwillig und drastisch ihr zuvor vielfältiges,
öffentliches Angebot aus Aktionen, Workshops und Vorträgen. "Für uns war das
eine selbstverständliche Entscheidung, wie auch die Masken- und
Abstandspflicht, zu der wir uns seit Beginn am 1. Juli freiwillig
verpflichteten", erinnert Schülerin Lara Sylla (16).

Mit dem Adventskalender möchten die Aktivist\*innen neue Formate ausprobieren,
um in der kalten Jahreszeit ohne Infektionsrisiko präsent zu bleiben. "Wir hoffen
mit diesem Experiment auch in diesen schweren Zeiten allen interessierten
Menschen einen Zugang zum Thema Klimagerechtigkeit bieten zu können", meint
Klimacamperin Paula Stoffels (18). "Die Ideen sind vielfältig und motivierend
und wir denken, es ist für alle etwas dabei."                                                            

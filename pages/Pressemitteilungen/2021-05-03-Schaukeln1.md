---
layout: page
title:  "3.5.2021: Klimacamper*innen wehren sich gegen Vorwurf von Oberbürgermeisterin Weber und hängen Schaukeln im Stadtgebiet auf"
date:   2021-05-03 02:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 42
---

*Pressemitteilung vom Augsburger Klimacamp am 3. Mai 2021*

# Klimacamper\*innen wehren sich gegen Vorwurf von Oberbürgermeisterin Weber und hängen Schaukeln im Stadtgebiet auf

In der kommenden Woche hängen Klimagerechtigkeitsaktivist\*innen des Augsburger
Klimacamps mit politischen Forderungsbannern beschmückte Schaukeln zur
allgemeinen Verwendung im Stadtgebiet auf. "Wertvoller öffentlicher Raum muss
nicht immer Autos gewidmet werden. Grünstreifen, Bänke, Spielplätze und Radwege
statt voller Autos strotzender Unorte!", erklärt Stefanie Bauer (18) die
symbolische Aktion.

Oberbürgermeisterin Weber erklärte kürzlich gegenüber der AZ: "Zuletzt wurde
eine Schaukel aufgehängt. Da fehlt es mir an der Vorstellungskraft, was das mit
Protest gegen den Klimawandel zu tun hat." [1] An Bäumen befestigten Schaukeln
kommt tatsächlich aber ein hoher Symbolwert zu: "Einerseits binden Bäume CO2
und sind so wichtige Verbündete im Kampf gegen die Klimakrise", so Bauer.
Andererseits: "Unsere Regierung versteht den Wert von Bäumen nicht und fällt
sie am laufenden Band, zuletzt für das 6,2 Millionen teure neue
Verkehrsleitsystem für Autos [2]. Die umstrittene Osttangente durch das Lechtal
ist auch noch nicht vom Tisch. Frau Weber lenkt mit ihren Äußerungen von
ihrer Klimapolitik des Nichtstuns und Abwartens ab."

"Anwohner\*innen können uns in den nächsten Tagen Bescheid geben, wo sie gerne
Schaukeln hätten. Wir kümmern uns dann um den Rest", so Bauer weiter. 

Die Schaukeln werden politische Botschaften zieren, die wie immer beim
Augsburger Klimacamp nicht Privatpersonen, sondern die Politik adressieren.
"Die Stadtregierung muss die Mobilitätswende anpacken, damit wir Privatpersonen
die Möglichkeit haben, ohne Kosten- oder Zeitnachteile auf Bus, Tram und Rad
umsteigen zu können."

[1] https://www.augsburger-allgemeine.de/augsburg/OB-Eva-Weber-im-Interview-Die-Politik-kann-nicht-auf-alles-Antworten-haben-id59600961.html<br>
[2] https://facebook.com/guterbaumaugsburg/ (Post vom 1.5.2021 10:03)

## Hinweis

Die Schaukeln werden mit speziellem PP-Splitfilm-Tauwerk (Durchmesser 14 mm)
mit einer Maximalbelastung von 3.000 kg an den Bäumen befestigt. Baumschoner
verhindern, dass die Bäume Schaden nehmen.

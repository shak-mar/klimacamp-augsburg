---
layout: page
title:  "22.09.2020: Aktivist*innen des Augsburger Klimacamps nehmen Stellung zu neuen Versammlungsauflagen"
date:   2020-09-22 22:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 19
---

*Pressemitteilung vom Augsburger Klimacamp am 22. September 2020*

# Aktivist\*innen des Augsburger Klimacamps nehmen Stellung zu neuen Versammlungsauflagen

Am heutigen Dienstag, den 22.09.2020, erließ die Versammlungsbehörde der Stadt
Augsburg neue Auflagen für das Klimacamp neben dem Rathaus.

Dazu berichtete die Augsburger Allgemeine [1], dass das Camp kleiner werden und
mehr Abstand zum Perlachturm und St. Peter halten müsse. Dies stimmt so nicht:
Die Größe der Versammlungsfläche ist mit 142 m² genau gleich geblieben, nur ist
die Versammlungsfläche jetzt zusammenhängend und nicht mehr aus vier kleineren
Teilen bestehend. Außerdem besteht der Abstand zum Perlachturm wie zuvor schon
aus dem Weg auf der Nordseite des Fischmarktes, dabei änderte sich nichts. Nach
dem Umbau wird ein Holzpavillon mit integrierter Photovoltaikanlage als
Sinnbild für Gebäude, die mehr Energe produzieren als sie benötigen,
prominenter sichtbar sein.

Klimacamperin Janika Pondorf (16) stimmt Frau Weber zu, „dass diese neuen
Auflagen im Gegensatz zu früheren Bescheiden kein Versuch sind, uns
loszuwerden. Hier geht es um besseren Brandschutz, und der ist im Interesse
aller. Verantwortung zu übernehmen ist für uns eine Selbstverständlichkeit. Aus
diesem Grund sind wir weiterhin fassungslos, dass die Stadt Augsburg ihrer
Verantwortung nicht gerecht wird und plant, das Dreifache der uns nach dem
Pariser Abkommen zustehenden CO₂-Menge auszustoßen [2]. Diese Ziele sind
unverantwortlich gegenüber den unterzeichnenden Ländern dieses Abkommens, und
insbesondere den Ländern des Globalen Südens.“ Die dort lebenden Menschen haben
am wenigsten zur Klimakrise beigetragen, doch werden am stärksten unter ihr
leiden, da sie sich anders als Augsburg kein teures
„Klimawandel-Anpassungskonzept“ [3] leisten können.

[1] [https://www.augsburger-allgemeine.de/augsburg/Stadt-Augsburg-macht-Auflagen-Klimacamper-muessen-sich-einschraenken-id58181041.html](https://www.augsburger-allgemeine.de/augsburg/Stadt-Augsburg-macht-Auflagen-Klimacamper-muessen-sich-einschraenken-id58181041.html)  
[2] [https://augsburg.klimacamp.eu/co2-budget/](https://augsburg.klimacamp.eu/co2-budget/)  
[3] [https://www.augsburger-allgemeine.de/augsburg/Der-globale-Klimawandel-wird-auch-Augsburg-hart-treffen-id58144621.html](https://www.augsburger-allgemeine.de/augsburg/Der-globale-Klimawandel-wird-auch-Augsburg-hart-treffen-id58144621.html)

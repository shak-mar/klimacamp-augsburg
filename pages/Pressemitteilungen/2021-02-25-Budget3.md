---
layout: page
title:  "25.02.2021: Das Klimacamp Augsburg begrüßt den Beschluss des CO2-Restbudgets im Stadtrat und weist Demokratiefeindlichkeitsvorwürfe zurück"
date:   2020-02-25 22:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 36
---

*Pressemitteilung vom Augsburger Klimacamp am 25. Februar 2021*

# Das Klimacamp Augsburg begrüßt den Beschluss des CO2-Restbudgets im Stadtrat und weist Demokratiefeindlichkeitsvorwürfe zurück

Seit 240 Tagen protestieren Klimagerechtigkeits-Aktivist\*innen neben dem
Augsburger Rathaus und fordern von Augsburgs Regierung, klimapolitisch
Verantwortung zu übernehmen. Nun beschloss der Stadtrat ein CO2-Restbudget von
9,7 Millionen Tonnen [1]. Damit wurde eine der Forderungen der Aktivist\*innen
erfüllt. Im April sollen bei einer Sonderstadtratssitzung dann auch konkrete
Maßnahmen beschlossen werden. Die Klimacamper\*innen hoffen, dass diese dem Ziel
gerecht werden und sozial gerecht umgesetzt werden.

"Nach dutzenden Tagen in der knallenden Sonne, eisigen Temperaturen, Sturm und
Niederschlägen bis zu den Knöchel, können wir klar von einem ersten großen
Erfolg sprechen", kommentiert Leon Ueberall (18) den Beschluss. "Wir freuen uns
sehr, dass sich Augsburg nun ein Ziel setzt, das mit der überlebenswichtigen
1,5-Grad-Grenze kompatibel ist. Dies ist ein wichtiger erster Schritt, der die
globale Realität planetarer Grenzen anerkennt."

Dieses Ziel erfordert weitreichende und schnelle Maßnahmen. Auf lokaler Ebene
ist dies beispielsweise die konsequente Umsetzung der Mobilitätswende, also
unter anderem der massive Ausbau und die massive Vergünstigung des öffentlichen
Personennahverkehrs, die Etablierung eines sicheren und durchgängigen
Radwegenetzes und die Ausschreibung von Tempo 30 im gesamten Stadtgebiet.
"Alles Maßnahmen, die auch unabhängig vom Klima Augsburg lebenswerter machen!",
so Ueberalls Mitstreiterin Stefanie Bauer (18).

Die Aktivist\*innen sind verwundert über den Vorwurf eines Stadtrates, dass ihr
Verhalten undemokratisch sei. Bauer: "Die letzten sieben Monate zeigten, dass
das Klimacamp den demokratischen Diskurs in Augsburg bei diesem wichtigen Thema
belebt hat. Sowohl am Fischmarkt als auch in der restlichen Stadtgesellschaft
gab es eine Vielzahl an kontroversen Diskussionen. Diese sind notwendig, denn
die Physik des Klimawandels stellt enormen Zeitdruck her und ist nicht
verhandelbar. Unsere Aufgabe als Bewegung ist es, diesen Diskurs anzutreiben.
Der konkrete Beschluss von Maßnahmen sowie deren Verhandlung ist Sache der
gewählten Vertreter\*innen im Stadtrat."

[1] https://ratsinfo.augsburg.de/bi/vo020.asp?VOLFDNR=10973

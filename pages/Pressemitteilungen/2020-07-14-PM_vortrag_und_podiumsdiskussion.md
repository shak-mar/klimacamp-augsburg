---
layout: page
title:  "14.07.2020: Vortrag zum Thema „100 % Erneuerbare Energieversorgung“ mit anschließender Podiumsdiskussion"
date:   2020-07-14 07:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 1
---

*Pressemitteilung vom Klimacamp am Rathaus vom 14.07.2020*

# Vortrag zum Thema „100 % Erneuerbare Energieversorgung“ mit anschließender Podiumsdiskussion

Wir, die Aktivist\*innen des Klimacamps neben dem Augsburger Rathaus, laden alle
Interessierten herzlich zu einem wissenschaftlichen Vortrag mit anschließender
Podiumsdiskussion und offener Diskussion ein. Dieser wird diesen Donnerstag
(16. Juli) um 19:00 Uhr im Klimacamp stattfinden. Wir hoffen auf einen regen
Austausch. Es wird genügend Möglichkeiten geben, die eigene Position zu
vertreten.

Der Eintritt ist frei. Die Parteien Grüne, Linke und ÖDP bestätigten bereits
ihre Teilnahme an der Podiumsdiskussion. Die Parteien CSU und SPD
signalisierten ebenfalls ihre Beteiligung, bestätigten diese aber noch nicht
abschließend.

## Thema des Vortrags:

100 % regenerative Stromerzeugung in Augsburg, in Bayern,
deutschlandweit: Impulsvortrag mit Dr. Peter Klafka (Energieingenieur,
Scientists for Future Aachen) mit anschließender Podiumsdiskussion
mit Politiker\*innen aus Augsburg

## Zusammenfassung:

Das Augsburger Klimacamp möchte weiter mit der Stadt Augsburg im Gespräch
bleiben und mit Expert\*innenwissen konstruktive Lösungen vorschlagen.
Die Energiewende ist ein wichtiger Teil des Wandels, der zur Eindämmung der
Klimakrise nötig ist.

Aber 100% erneuerbar, (wie) kann das funktionieren? Wie viel Zeit bleibt
uns noch für die Energiewende? Wird die Wende unbezahlbar oder doch eher
preiswert? Und wie kann die Stadt Augsburg ihren Teil dazu beitragen?

Wenn Ihr Eure eigene Einschätzung zur Energiewende in der Stromversorgung
finden oder überdenken wollt, Aussagen von anderen einordnen und
Expert\*innenaussagen nachvollziehen sowie die Meinungen der Stadträt\*innen
hören möchtet, dann kommt zum Vortrag ins Klimacamp.

Vermittelt wird Wissen unter anderem in diesen Bereichen:
- CO2-freie bzw. arme Erzeugungstechniken und ihre Eigenschaften
- Wirkungsweise der Erzeugung mit Sonne, Wind, Wasser, Biomasse, Kohle, Gas, Kernenergie
- Speichermöglichkeiten: Techniken, Nutzen, Aufwand, benötigte Kapazität

*Wo soll denn der Strom herkommen während einer Dunkelflaute? Sonne und Wind*
*seien viel zu teuer. Beim Bau eines Solarkraftwerkes werde mehr Strom*
*verbraucht als man damit erzeuge. Die Kohlekraft sei ein flexibler Partner*
*der erneuerbaren Energien.*  
Zu allen diesen Behauptungen gibt es plausible wissenschaftliche Antworten.
Dr. Peter Klafka, welcher in Energiewirtschaft promovierte und bei den
„Scientist for Future“ aktiv ist, wird uns in seinem Vortrag am 16. Juli im
Klimacamp (Am Fischmarkt, direkt neben dem Rathaus) genau diese Antworten
geben. Anschließend diskutieren wir mit Euch und Augsburger Politiker\*innen
über die Umsetzung der Energiewende.

Viele Grüße
Ihre Aktivist\*innen vom Augsburger Klimacamp

PS: Und am Mittwochabend (15.7.) stellt Michael Finsinger das Konzept „Verkehr
4.0 für den Ballungsraum Augsburg“ vor. Die Verkehrswende zu echter Mobilität
für alle – schnell umsetzbar – effizient – gerecht – klimafreundlich. 19:00
Uhr, Eintritt frei, am Klimacamp.

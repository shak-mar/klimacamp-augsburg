---
layout: page
title:  "07.12.2020: Stadt verhöhnt Catcalls of Augsburg mit Polizei- und Feuerwehreinsatz – Klimacamp bekundet Solidarität"
date:   2020-12-07 23:30:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 29
---

*Pressemitteilung vom Augsburger Klimacamp am 7. Dezember 2020*

# Stadt verhöhnt Catcalls of Augsburg mit Polizei- und Feuerwehreinsatz -- Klimacamp bekundet Solidarität

**[Zur Stellungnahme von Catcalls of Augsburg](https://www.instagram.com/p/CIhMITwBYD0)**

Die Gruppe "Catcalls of Augsburg" dokumentiert mit Straßenkreide
Vorfälle sexueller Belästigung direkt an den Orten der Übergriffe; die
Augsburger Allgemeine Zeitung berichtete vor kurzem ausführlich über das
zivilgesellschaftliche Engagement der beteiligten Frauen [1]. Die
Aktivist\*innen des Augsburger Klimacamps wurden heute Zeuge einer
"beispiellosen Aktion der Verhöhnung" der Stadt gegen Catcalls of
Augsburg.

Eine Kreideschriftzug von Catcalls of Augsburg vor dem Rathaus machte
heute ab etwa 15:30 Uhr alle Passant\*innen auf einen kürzlich dort
erfolgten sexuellen und rassistischen Übergriff aufmerksam (siehe
[Foto zur freien Verwendung](https://www.speicherleck.de/iblech/stuff/catcalling.jpeg)).

Um 17:40 Uhr kamen zwei Polizist\*innen ins Protestcamp und erkundigten
sich nach Hinweisen über die Verantwortlichen der Kreideaktion. "Nachdem
ich auf Catcalls of Augsburg verwies, bat die Polizei mich, trotzdem den
Schriftzug zu entfernen. Passant\*innen hätten sich von diesem belästigt
gefühlt", so Klimacamper Ingo Blechschmidt (32).

Als die Aktivist\*innen des Camps der Bitte nicht Folge leisteten, da sie
keinerlei rechtliche Verpflichtung dazu hatten sowie die Aktion
unterstützten, beantragte die Polizei Amtshilfe bei der Feuerwehr. "Ein
großes Löschfahrzeug sowie drei Polizeiautos rückten an, mindestens fünf
Feuerwehrpersonen und vier Polizist\*innen waren im Einsatz und spritzten
Löschwasser über den Kreideschriftzug", so Blechschmidt weiter.

"Offensichtlich fehlt es an einer Sensibilisierung für
Verhältnismäßigkeit, sowie Verständnis für freie Meinungsäußerung", so
Blechschmidts Mitstreiterin Gwendolyn Rautenberg (19). "Ist das die
Antwort auf zivilgesellschaftliches Engagement, das auf das wichtige
Problem der sexualisierten Gewalt aufmerksam macht? Diese Überreaktion
der Stadt fügt sich leider in eine lange Liste von Beispielen
unnachvollziehbarer Prioritätensetzung ein. Die Verwendung von
Straßenkreide auf Gehwegen ist eine Meinungsäußerung, die unter
verfassungsrechtlichem Schutz steht. Insbesondere gilt dies, solange die
Kreide vom Regen wieder weggewaschen wird sowie der Verkehr nicht
gefährdet wird. Wieso ließ sich die Polizei hier überhaupt involvieren?"

Bei einer großflächigen Kreideaktion von Abtreibungsgegner\*innen am
27.9.2020 neben dem Manzù-Brunnen am Königsplatz intervenierten
Beamt\*innen der Stadt dagegen nicht und ließen die
frauenrechtsfeindlichen Sprüche stehen, bis sie schließlich von
Feminist\*innen am nächsten Tag eigenhändig entfernt wurden. "Diese
ungleiche Reaktion zeigt, dass noch viel feministische und
antirassistische Arbeit vor uns liegt — nicht nur in der Breite der
Gesellschaft, sondern auch in den staatlichen Entscheidungsstrukturen."

"Wenn wir Regierungspolitiker\*innen auf die dringende Notwendigkeit
ansprechen, das CO2-Budget der Stadt nicht zu überschreiten, werden wir
immer auf den knappen Stadthaushalt verwiesen", so Rautenberg weiter.
"Mit den Kosten des heutigen Polizei- und Feuerwehreinsatzes hätte die
Stadt schon eine kleine Klimagerechtigkeitsmaßnahme finanzieren können,
etwa die Montage einiger Fahrradständer an einer Tramhaltestelle."

Der Einsatz für Klimagerechtigkeit und Feminismus sowie Antirassismus
sind untrennbar miteinander verbunden. Frauen und BIPoC sind im
statistischen Durchschnitt stärker von der Klimakrise betroffen, tragen
weniger zu ihr bei und sind seltener in Entscheidungspositionen, um der
Klimakrise zu begegnen. Das Augsburger Klimacamp versteht sich seit
seiner Errichtung klar als queerfeministisch und spricht Catcalls of
Augsburg seine volle Solidarität aus. "Catcalls of Augsburg leistet
wertvolle Aufklärungsarbeit. Sie offenbart insbesondere Männern, die in
ihrem gesamtem Leben kaum mit Alltagssexismus konfrontiert sind, wie
viel sexualisierte Gewalt Frauen erleben. Auch und insbesondere hier in
Augsburg, an öffentlichen Plätzen", erklärt Rautenberg.

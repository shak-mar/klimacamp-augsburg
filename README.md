# Klimacamp Augsburg
![Logo vom Klimacamp](logo.png)
Hier ist der Quellcode der [Website vom Klimacamp Augsburg](https://klimacamp-augsburg.de/).

## Übersicht

* [Jekyll](https://jekyllrb.com/) baut aus dem Quellcode die Website (Static renderer)
* [Vercel](https://github.com/vercel/vercel) führt das automatische Bauen durch (CI/CD)

## Quickstart - Redaktionelles

* Du brauchst dazu einen GitLab-Account - [hier klicken zum Registrieren](https://gitlab.com/users/sign_up).
* Um eine Seite zu bearbeiten, such sie Dir oben in der Liste der Dateien raus
* Klicke dann auf `Edit`. Du kannst die Datei dann bearbeiten
  und Deine Änderungen einreichen. Mach erst Deine Änderungen und fülle dann unten
  die Überschrift Deiner Änderungen und ggf. eine längere Beschreibung aus, falls
  Du dazu etwas erklären willst.
* Klicke dann unten auf *Propose changes*.

Du arbeitest da im Hintergrund mit einer *Kopie* der Website. Du kannst also nichts
kaputt machen. Deine Änderungen werden vor Übernahme auf die Seite erst noch
genehmigt - nicht aus autoritären Gründen sondern aus technischen. So funktioniert
GitHub. Alle arbeiten an ihren eigenen Kopien und eine ist eben diejenige,
die man auch im Internet sieht.

[Hier gibt es eine Anleitung für Formatierung und so.](https://guides.github.com/features/mastering-markdown/).
